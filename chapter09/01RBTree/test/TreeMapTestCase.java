import java.util.TreeMap;

import junit.framework.TestCase;


public class TreeMapTestCase extends TestCase {
    private TreeMap<Integer, Integer> mTree;

    @Override
    protected void setUp() throws Exception {
        mTree = new TreeMap<Integer, Integer>();
        super.setUp();
    }

    // 插入两个红色节点
    public void test1() {
        mTree.put(50, 0);
        mTree.put(30, 0);
        mTree.put(70, 0);
        mTree.put(20, 0);
        mTree.put(60,0);
        System.out.println("size = " + mTree.size());
    }

    // 满树
    public void test2() {
        mTree.put(50, 0);
        mTree.put(25, 0);
        mTree.put(75, 0);
        mTree.put(12, 0);
        mTree.put(38, 0);
        mTree.put(62, 0);
        mTree.put(87, 0);
        mTree.put(6, 0);
        mTree.put(18, 0);
        mTree.put(35, 0);
        mTree.put(48, 0);
        mTree.put(56, 0);
        mTree.put(68, 0);
        mTree.put(80, 0);
        mTree.put(93, 0);
        mTree.put(1, 0);
        mTree.put(9, 0);
        mTree.put(15, 0);
        mTree.put(23, 0);
        mTree.put(28, 0);
        mTree.put(37, 0);
        mTree.put(39, 0);
        mTree.put(49, 0);
        mTree.put(53, 0);
        mTree.put(59, 0);
        mTree.put(65, 0);
        mTree.put(71, 0);
        mTree.put(77, 0);
        mTree.put(83, 0);
        mTree.put(90, 0);
        mTree.put(97, 0);

        System.out.println("size = " + mTree.size());
    }

    // 插入一个新节点，P是黑色
    public void test3() {
        mTree.put(50, 0);
        mTree.put(25, 0);
        mTree.put(75, 0);
        mTree.put(12, 0);
        mTree.put(38, 0);
        mTree.put(62, 0);
        mTree.put(87, 0);
        mTree.put(6, 0);
        // 此处断点
        mTree.put(18, 0);

        System.out.println("size = " + mTree.size());
    }

    // 插入一个新节点，P是红色，X是G的外侧结点
    public void test4() {
        mTree.put(50, 0);
        mTree.put(25, 0);
        mTree.put(75, 0);
        mTree.put(12, 0);
        mTree.put(62, 0);
        mTree.put(87, 0);
        // 此处断点
        mTree.put(6, 0);

        System.out.println("size = " + mTree.size());
    }

    // 插入一个新节点，P是红色，X是G的一个内侧子孙结点
    public void test5() {
        mTree.put(50, 0);
        mTree.put(25, 0);
        mTree.put(75, 0);
        mTree.put(12, 0);
        mTree.put(18, 0);

        System.out.println("size = " + mTree.size());
    }

    public void test6() {
        mTree.put(50, 0);
        mTree.put(25, 0);
        mTree.put(75, 0);
        mTree.put(12, 0);
        mTree.put(37, 0);
        mTree.put(6, 0);
        mTree.put(18, 0);

    }
}

