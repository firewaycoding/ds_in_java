package fireway;

/**
 * 归并两个有序数组
 * 2018年 06月 28日 星期四
 *
 * @author fireway
 */
public class IntArrays {
    private static final boolean DEBUG = false;
    /**
     * merge a and b to c
     */
    public static void merge(int[] a, int lenA, int [] b, int lenB, int[] c) {
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < lenA && j < lenB) {
            if (a[i] < b[j]) {
                c[k++] = a[i++];
            } else if (a[i] > b[j]) {
                c[k++] = b[j++];
            }
        }

        if (DEBUG) {
            System.out.println("i[" + i + "], j[" + j + "], k[" + k + "]");
        }

        while (i < lenA) {
            c[k++] = a[i++];
        }

        while (j < lenB) {
            c[k++] = a[i++];
        }
    }
}
