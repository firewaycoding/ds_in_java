package test;

import java.util.Arrays;

import fireway.IntArrays;
import junit.framework.TestCase;

/**
 * 归并两个有序数组
 * 2018年 06月 28日 星期四
 *
 * @author fireway
 */
public class IntArrayTestCase extends TestCase {
    public void test1() {
        int[] a = {23, 47, 81, 95};
        int aLen = a.length;
        int[] b = {7, 14, 39, 55, 62, 74};
        int bLen = b.length;
        int cLen = aLen + bLen;
        int[] c = new int[cLen];
        IntArrays.merge(a, aLen, b, bLen, c);
        System.out.println(Arrays.toString(c));
    }

    public void test2() {
        int[] a = {7, 14, 39, 55, 62, 74};
        int aLen = a.length;
        int[] b = {23, 47, 81, 95};
        int bLen = b.length;
        int cLen = aLen + bLen;
        int[] c = new int[cLen];
        IntArrays.merge(a, aLen, b, bLen, c);
        System.out.println(Arrays.toString(c));
    }
}
