package test;

import junit.framework.TestCase;
import fireway.Gcd;

/**
 * 最大公约数的递归
 *
 * 2018年 06月 17日 星期日
 *
 * @author fireway
 */
public class GcdTestCase extends TestCase {
    public void test1() {
        Gcd gcd = new Gcd();
        int m = 140;
        int n = 21;
        int result = gcd.divisor1(m, n);
        System.out.println("(" + m + "," + n + ")=" + result);
    }

    public void test2() {
        Gcd gcd = new Gcd();
        int m = 21;
        int n = 140;
        int result = gcd.divisor1(m, n);
        System.out.println("(" + m + "," + n + ")=" + result);
    }
}
