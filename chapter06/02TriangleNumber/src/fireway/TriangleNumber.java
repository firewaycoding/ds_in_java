package fireway;

/**
 * 三角数学
 * 2018年 06月 14日 星期四
 *
 * @author fireway
 */
public class TriangleNumber {
    public int calcN(int n) {
        if (1 == n) {
            return 1;
        } else {
            return n + calcN(n - 1);
        }
    }
}
