package fireway;

/**
 * 汉诺（Hanoi）塔问题
 *
 * 2018年 06月 26日 星期二
 * @author fireway
 */
public class Tower {
    public static void move(int layer, char from, char inter, char to) {
        if (layer == 1) {
            System.out.println("Disk 1 from " + from + " to "+ to);
        } else {
            move(layer - 1, from, to, inter);
            System.out.println("Disk " + layer + " from " + from + " to " + to);
            move(layer - 1, inter, from, to);
        }
    }
}
