package test;

import junit.framework.TestCase;

/**
 * 求一个数的乘方
 * 2018年 07月 08日 星期日
 *
 * @author fireway
 */
public class MathTestCase extends TestCase {
    public void test1() {
        int result = fireway.Math.pow(2, 16);
        System.out.println("result = " + result);
        assertEquals(65536, result);
    }

    public void test2() {
        int result = fireway.Math.pow(3, 18);
        System.out.println("result = " + result);
        assertEquals(387420489, result);
    }
}
