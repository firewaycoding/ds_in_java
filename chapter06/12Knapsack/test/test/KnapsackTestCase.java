package test;

import junit.framework.TestCase;
import fireway.Knapsack;

/**
 * <p> 写一个能解决背包问题的程序，任意给定背包的容量以及一些列物品的重量，设把这些重量值存在一个数组中。 </p>
 * <p> 提示：递归方法knapsack()的参数是目标重量和剩下物品开始位置的数组下标。 </p>
 * <p> 2018年 07月 09日 星期一 </p>
 *
 * @author fireway
 */
public class KnapsackTestCase extends TestCase {
    public void test1() {
        int[] array = {11, 8, 7, 6, 5};
        int total = 20;
        Knapsack k = new Knapsack(array);
        k.knapsack(total, 0);
    }

    public void test2() {
        int[] array = {20, 18, 16, 14, 12, 10, 8, 6, 4, 2};
        int total = 30;
        Knapsack k = new Knapsack(array);
        k.knapsack(total, 0);
    }
}

