package test;

import fireway.TriangleNumber;
import junit.framework.TestCase;

/**
 * 三角数学
 * 2018年 06月 14日 星期四
 *
 * @author fireway
 */
public class TriangleNumberTestCase extends TestCase {
    public void test() {
        TriangleNumber t = new TriangleNumber();
        int input = 5;
        System.out.println("Enter a number = " + input);
        System.out.println("\n");
        int n = t.calcN(input);
        System.out.println("\n");
        System.out.println("Triangle = " + n);
    }
}
