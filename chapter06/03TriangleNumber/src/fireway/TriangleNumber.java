package fireway;

/**
 * 三角数学
 * 2018年 06月 14日 星期四
 *
 * @author fireway
 */
public class TriangleNumber {
    public int calcN(int n) {
        System.out.println("Entering n = " + n);
        if (1 == n) {
            System.out.println("Returning 1");
            return 1;
        } else {
            int tmp = n + calcN(n - 1);
            System.out.println("Returning " + tmp);
            return tmp;
        }
    }
}
