package test;

import fireway.Combination;
import junit.framework.TestCase;

/**
 * 应用一个递归的方法来显示由一群人组队的所有可能方案（由n个每次挑k个）
 * 2018年 07月 14日 星期六
 *
 * @author fireway
 */
public class CombinationTestCase extends TestCase {
    public void test1() {
        char[] persons = {'A','B','C','D','E'};
        Combination cb = new Combination(persons);
        cb.showTeams(3);
    }
}
