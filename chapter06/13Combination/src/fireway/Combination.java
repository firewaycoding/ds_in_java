package fireway;

/**
 * 应用一个递归的方法来显示由一群人组队的所有可能方案（由n个每次挑k个）
 * 2018年 07月 14日 星期六 
 *
 * @author fireway
 */
public class Combination {
    private char[] mPersons;

    private boolean[] mSelects;  // 标记成员选中与否

    public Combination(char[] persons) {
        mPersons = persons;
        mSelects = new boolean[persons.length];
    }

    public void showTeams(int teamNumber) {
        showTeams(teamNumber, 0);
    }

    /**
     * 找所有可能的解
     *
     * @param teamNumber 需要选择的队员数
     * @param index 从第几个队员开始选择
     */
    private void showTeams(int teamNumber, int index) {
        if (0 == teamNumber) {
            for (int i = 0; i < mSelects.length; i++) {
                if (mSelects[i]) {
                    System.out.print(mPersons[i] + " ");
                }
            }
            System.out.println();
            return;
        }

        // index超过可供选择的人的数,未找到
        if (index >= mPersons.length) {
            return;
        }

        mSelects[index] = true;
        showTeams(teamNumber - 1, index + 1);
        mSelects[index] = false;
        showTeams(teamNumber, index + 1);
    }
}
