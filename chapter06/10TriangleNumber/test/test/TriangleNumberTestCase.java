package test;

import fireway.TriangleNumber;
import junit.framework.TestCase;

/**
 * 三角数学的递归转化为基于栈的方法示例
 * 2018年 07月 08日 星期日
 *
 * @author fireway
 */
public class TriangleNumberTestCase extends TestCase {
    public void test() {
        TriangleNumber t = new TriangleNumber();
        int n = t.calcN(5);
        System.out.println("n = " + n);
    }
}
