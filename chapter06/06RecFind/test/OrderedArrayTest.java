/**
 * 升序数组
 * 2018年 06月 21日 星期四
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class OrderedArrayTest extends TestCase {
    public void test1() {
        AscOrderArray ascOrderArray = new AscOrderArray(100);
        ascOrderArray.insert(77);
        ascOrderArray.insert(99);
        ascOrderArray.insert(44);
        ascOrderArray.insert(55);
        ascOrderArray.insert(22);
        ascOrderArray.insert(88);
        ascOrderArray.insert(11);
        ascOrderArray.insert(0);
        ascOrderArray.insert(66);
        ascOrderArray.insert(33);

        ascOrderArray.display();
        int result = ascOrderArray.find(73);
        System.out.println("result = " + result);
    }

    public void test2() {
        AscOrderArray ascOrderArray = new AscOrderArray(100);
        ascOrderArray.insert(77);
        ascOrderArray.insert(99);
        ascOrderArray.insert(44);
        ascOrderArray.insert(55);
        ascOrderArray.insert(22);
        ascOrderArray.insert(88);
        ascOrderArray.insert(11);
        ascOrderArray.insert(0);
        ascOrderArray.insert(66);
        ascOrderArray.insert(33);

        ascOrderArray.display();

        long[] keys = { 98, 0, 66, 57 };
        for (long k : keys) {
            int index = ascOrderArray.find(k);
            System.out.println("find " + k + " index " + index);
        }

        long[] values = { 77, 99, 44, 55, 22, 88, 11, 0, 66, 33 };
        for (long value : values) {
            if (ascOrderArray.delete(value)) {
                System.out.println("delete " + value + " ok");
            } else {
                System.out.println("delete " + value + " fail");
            }
        }

        ascOrderArray.display();
    }

    /**
     * 边界测试： 超出数组大小场景
     */
    public void test3() {
        AscOrderArray ascOrderArray = new AscOrderArray(8);
        ascOrderArray.insert(77);
        ascOrderArray.insert(99);
        ascOrderArray.insert(44);
        ascOrderArray.insert(55);
        ascOrderArray.insert(22);
        ascOrderArray.insert(88);
        ascOrderArray.insert(11);
        ascOrderArray.insert(0);
        ascOrderArray.insert(66);
        ascOrderArray.insert(33);
    }

    /**
     * 边界测试： 删除数组第1个元素
     */
    public void test4() {
        AscOrderArray ascOrderArray = new AscOrderArray(8);
        ascOrderArray.insert(77);
        ascOrderArray.insert(99);
        ascOrderArray.insert(44);
        ascOrderArray.insert(55);
        ascOrderArray.insert(22);
        ascOrderArray.insert(88);
        ascOrderArray.insert(11);
        ascOrderArray.insert(0);

        ascOrderArray.display();

        boolean b = ascOrderArray.delete(0);
        System.out.println("delete = " + b);
        ascOrderArray.display();
    }

    /**
     * 边界测试： 删除数组最后一个元素
     */
    public void test5() {
        AscOrderArray ascOrderArray = new AscOrderArray(8);
        ascOrderArray.insert(77);
        ascOrderArray.insert(99);
        ascOrderArray.insert(44);
        ascOrderArray.insert(55);
        ascOrderArray.insert(22);
        ascOrderArray.insert(88);
        ascOrderArray.insert(11);
        ascOrderArray.insert(0);

        ascOrderArray.display();

        boolean b = ascOrderArray.delete(99);
        System.out.println("delete = " + b);
        ascOrderArray.display();
    }
}

