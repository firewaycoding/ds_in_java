package fireway;

/**
 * 三角数学
 * 2018年 06月 13日 星期三
 *
 * @author fireway
 */
public class TriangleNumber {
    public int calcN(int n) {
        int total = 0;

        // 循环了N次，total值在第一次循环中加N，第二次循环中加N-1，如此循环一直加到1，
        // 当N减少到0时推出循环。
        while (n > 0) {  // util n is 1
            total += n;
            n--;
        }

        return total;
    }
}
