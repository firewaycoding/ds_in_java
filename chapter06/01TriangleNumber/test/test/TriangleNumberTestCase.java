package test;

import fireway.TriangleNumber;
import junit.framework.TestCase;

/**
 * 三角数学
 * 2018年 06月 13日 星期三
 *
 * @author fireway
 */
public class TriangleNumberTestCase extends TestCase {
    public void test() {
        TriangleNumber t = new TriangleNumber();
        int n = t.calcN(5);
        System.out.println("n = " + n);
    }
}
