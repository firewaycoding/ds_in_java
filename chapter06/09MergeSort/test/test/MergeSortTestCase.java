package test;

import fireway.MergeSort;
import junit.framework.TestCase;

/**
 * <p> 归并排序 </p>
 * 2018年 07月 03日 星期二
 *
 * @author fireway
 */
public class MergeSortTestCase extends TestCase {
    public void test1() {
        MergeSort arr = new MergeSort(10);

        arr.insert(7);
        arr.insert(9);
        arr.insert(4);
        arr.insert(5);
        arr.insert(2);
        arr.insert(8);
        arr.insert(1);
        arr.insert(0);
        arr.insert(6);
        arr.insert(3);

        arr.display();

        arr.mergeSort();

        arr.display();
    }

    public void test2() {
        MergeSort arr = new MergeSort(4);

        arr.insert(77);
        arr.insert(99);
        arr.insert(44);
        arr.insert(55);

        arr.display();

        arr.mergeSort();

        arr.display();
    }

    public void test3() {
        MergeSort arr = new MergeSort(3);

        arr.insert(77);
        arr.insert(99);
        arr.insert(44);

        arr.display();

        arr.mergeSort();

        arr.display();
    }
}
