package com.test;

import java.util.LinkedList;
import java.util.ListIterator;

import junit.framework.TestCase;

/**
 * JDK1.6的LinkedList的迭代器测试用例
 * 2018年 05月 26日 星期六
 *
 * @author fireway
 */
public class ListIteratorTestCase extends TestCase {
    public void test() {
        LinkedList<Integer> list = new LinkedList<Integer>();
        ListIterator<Integer> iterator = list.listIterator();

        iterator.add(21);
        iterator.add(40);
        iterator.add(30);
        iterator.add(7);
        iterator.add(45);

        System.out.println(list + ", whether has next? " + iterator.hasNext());

        while (iterator.hasNext()) {
            Integer e = iterator.next();
            if (e % 3 == 0) {
                iterator.remove();
            }
        }

        System.out.println(list);
    }
}
