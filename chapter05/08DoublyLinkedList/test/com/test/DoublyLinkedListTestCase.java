package com.test;

import com.fireway.DoublyLinkedList;

import junit.framework.TestCase;

/**
 * 基于双向链表的双端链表
 * 2018年 05月 18日 星期五
 *
 * @author fireway
 */
public class DoublyLinkedListTestCase extends TestCase {
    public void test1() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();

        list.addFirst(22);
        list.addFirst(44);
        list.addFirst(66);
        list.addLast(11);
        list.addLast(33);
        list.addLast(55);

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());

        // Integer result = list.removeFirst();
        Integer result = list.removeLast();
        System.out.println(result);

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());
    }

    public void test2() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();

        list.addFirst(22);
        list.addFirst(44);
        list.addFirst(66);

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());
    }

    public void test3() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();

        list.addLast(11);

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());

        list.removeFirst();

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());
    }

    public void test4() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();

        list.addFirst(11);
        list.addLast(11);

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());

        // list.removeLast();
        list.remove(11);

        list.displayForward();
        list.displayBackward();
        System.out.println("size = " + list.size());
    }

    public void test5() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();

        list.addFirst(22);
        list.addFirst(44);
        list.addFirst(66);
        list.addLast(11);
        list.addLast(33);
        list.addLast(55);

        list.displayForward();
        // list.displayBackward();
        System.out.println("size = " + list.size());

        boolean result = list.remove(23);
        System.out.println(result);

        list.displayForward();
        // list.displayBackward();
        System.out.println("size = " + list.size());

        Integer o = list.get(5);
        System.out.println("index 5 = " + o);

        int index = list.indexOf(11);
        System.out.println("object 11 = " + index);
    }

    public void test6() {
        DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();

        list.addFirst(22);
        list.addFirst(44);
        list.addFirst(66);
        list.addLast(11);
        list.addLast(33);
        list.addLast(55);

        list.displayForward();
        System.out.println("size = " + list.size());

        Integer o = list.get(3);
        System.out.println("index 3 = " + o);

        list.add(3, 77);
        list.displayForward();
        System.out.println("size = " + list.size());
    }
}
