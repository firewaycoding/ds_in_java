package com.test;

import com.fireway.Queue;

import junit.framework.TestCase;

/**
 * 2018年 05月 03日 星期四
 *
 * @author fireway
 */
public class QueueTestCast extends TestCase {
    public void test() {
        Queue q = new Queue();
        q.add(20);
        q.add(40);
        System.out.println(q);

        q.add(60);
        q.add(80);
        System.out.println(q);

        q.remove();
        q.remove();
        q.remove();
        q.remove();
        // !q.remove();
        System.out.println(q);
    }
}
