package com.test;

import com.fireway.SortedLinkedList;

import junit.framework.TestCase;

/**
 * 有序单链表
 * 2018年 05月 05日 星期六
 *
 * @author fireway
 */
public class SortedLinkedListTestCase extends TestCase {
    public void test1() {
        SortedLinkedList<Integer> list = new SortedLinkedList<Integer>();

        list.add(40);
        list.add(20);
        list.add(10);
        list.add(50);

        System.out.println(list);

        list.add(70);
        list.add(60);
        list.add(30);
        System.out.println(list);

        list.remove();
        list.remove();
        System.out.println(list);
    }

    public void test2() {
        SortedLinkedList<String> list = new SortedLinkedList<String>();

        list.add("Red");
        list.add("Orange");
        list.add("Yellow");
        list.add("Green");

        System.out.println(list);

        list.add("Cyan");
        list.add("Blue");
        list.add("Purple");
        System.out.println(list);

        list.remove();
        list.remove();
        System.out.println(list);
    }
}
