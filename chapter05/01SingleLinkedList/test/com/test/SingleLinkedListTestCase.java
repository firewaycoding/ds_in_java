package com.test;

import junit.framework.TestCase;

import com.fireway.SingleLinkedList;

/**
 * 单链表测试用例
 * 2018年 04月 26日 星期四
 *
 * @author fireway
 */
public class SingleLinkedListTestCase extends TestCase {
    public void test1() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");

        while (!list.empty()) {
            String e = list.removeFirst();
            System.out.println(e);
        }
    }

    public void test2() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");

        String e = list.removeFirst();
        System.out.println(e);

        e = list.removeFirst();
        System.out.println(e);

        e = list.removeFirst();
        System.out.println(e);
    }
}

