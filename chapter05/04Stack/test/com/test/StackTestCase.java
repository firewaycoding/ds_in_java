package com.test;

import com.fireway.Stack;

import junit.framework.TestCase;

/**
 * 2018年 05月 03日 星期四
 *
 * @author fireway
 */
public class StackTestCase extends TestCase {
    public void test1() {
        Stack stack = new Stack();
        stack.push(20);
        stack.push(40);

        System.out.println(stack);

        stack.push(60);
        stack.push(80);

        System.out.println(stack);

        stack.pop();
        stack.pop();

        System.out.println(stack);
    }

    public void test2() {
        Stack stack = new Stack();
        stack.push(20);
        stack.push(40);

        System.out.println(stack);

        stack.push(60);
        stack.push(80);

        System.out.println(stack);

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();

        System.out.println(stack);
    }
}
