package com.fireway;

import java.util.NoSuchElementException;

/**
 * 双端链表
 * 2018年 04月 30日 星期一
 *
 * @author fireway
 */
public class DoubleEndedList<E> {
    private Entry<E> mHeader;  // 表头

    private Entry<E> mTailer;  // 表尾

    private int mSize = 0;

    public DoubleEndedList() {
        mHeader = null;
        mTailer = null;
    }

    /**
     * Inserts the specified element at the beginning of this list.
     *
     * @param e the element to add
     */
    public void addFirst(E e) {
        Entry<E> newEntry = new Entry<E>(e);
        if (empty()) {
            mTailer = newEntry;
        }
        newEntry.mNext = mHeader;
        mHeader = newEntry;
        mSize++;
    }

    /**
     * Appends the specified element to the end of this list.
     */
    public void addLast(E e) {
        Entry<E> newEntry = new Entry<E>(e);
        if(empty()) {
            mHeader = newEntry;
        } else {
            mTailer.mNext = newEntry;
        }
        mTailer = newEntry;
        mSize++;
    }

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list
     */
    public E removeFirst() {
        if (empty()) {
            throw new NoSuchElementException();
        } else {
            Entry<E> tmp = mHeader;
            E result = tmp.mElement;
            mHeader = tmp.mNext;
            if (null == mHeader) {
                mTailer = null;
            }
            mSize--;
            return result;

        }
    }

    public boolean empty() {
        return null == mHeader && null == mTailer;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     */
    public int indexOf(Object o) {
        int index = 0;
        for (Entry<E> e = mHeader; e != null ; e = e.mNext) {
            if (isEqual(o, e.mElement)) {
                return index;
            }
            index++;
        }

        return -1;
    }

    /**
     * Returns the element at the specified position in this list.
     * @param index index of the element to return
     * @return the element at the specified position in this list
     */
    public E get(int index) {
        if (index < 0 || index >= mSize) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + mSize);
        }

        Entry<E> e = mHeader;
        for (int i = 0; i < index; i++) {
            e = e.mNext;
        }

        return e.mElement;
    }

    public boolean remove(Object o) {
        Entry<E> current = mHeader;
        Entry<E> previous = null;

        while (current != null) {
            if (isEqual(o, current.mElement)) {
                break;
            }
            previous = current;
            current = current.mNext;
        }

        if (null == previous) {
            mHeader = mHeader.mNext;
            if (null == mHeader) {
                mTailer = null;
            }

        } else if (null == current) {
            return false;
        } else {
            previous.mNext = current.mNext;
        }

        mSize--;
        return true;
    }

    private boolean isEqual(Object o, E element) {
        if (null == o) {
            return element == null;
        } else {
            return o.equals(element);
        }
    }

    /**
     * Returns the number of elements in this list.
     */
    public int size() {
        return mSize;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("[");
        for (int i = 0; i < mSize; i++) {
            E e = get(i);
            sb.append(e);
            if (i != mSize - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }

    private static class Entry<E> {
        private E mElement;

        /* package */ Entry<E> mNext;

        public Entry(E element) {
            mElement = element;
        }
    }
}
