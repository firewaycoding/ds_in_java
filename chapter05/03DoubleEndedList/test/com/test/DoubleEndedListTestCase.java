package com.test;

import junit.framework.TestCase;

import com.fireway.DoubleEndedList;

/**
 * 双端链表测试用例
 * 2018年 04月 30日 星期一
 *
 * @author fireway
 */
public class DoubleEndedListTestCase extends TestCase {
    public void test1() {
        DoubleEndedList<String> list = new DoubleEndedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");

        list.addLast("Green");
        list.addLast("Cyan");
        list.addLast("Blue");

        System.out.println(list.size());
        System.out.println(list);
        while (!list.empty()) {
            System.out.print(list.removeFirst() + ' ');
        }
    }

    public void test2() {
        DoubleEndedList<String> list = new DoubleEndedList<String>();
        list.addLast("Green");
        list.addLast("Cyan");
        list.addLast("Blue");

        System.out.println(list);

        list.removeFirst();
        list.removeFirst();
        list.removeFirst();

        System.out.println(list);
    }

    public void test3() {
        DoubleEndedList<String> list = new DoubleEndedList<String>();
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");

        System.out.println(list);

        list.removeFirst();
        list.removeFirst();
        list.removeFirst();

        System.out.println(list);
    }

    public void test4() {
        DoubleEndedList<String> list = new DoubleEndedList<String>();
        boolean result = list.empty();
        System.out.println(result);
        list.addFirst("Red");
        result = list.empty();
        System.out.println(result);
        list.removeFirst();
        result = list.empty();
        System.out.println(result);

    }
}

