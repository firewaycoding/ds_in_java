package com.fireway;

/**
 * 链表插入排序(List Insertion Sort)
 * 2018年 05月 06日 星期日
 *
 * @author fireway
 */
public class LinkedListSort {
    private long mLArray[];

    private SortedLinkedList<Long> mSortedLinkedList = new SortedLinkedList<Long>();

    private int mSize;

    public LinkedListSort(int initialCapacity) {
        mLArray = new long[initialCapacity];
        mSize = 0;
    }

    public void insert(long value) {
        mLArray[mSize++] = value;
    }

    public void display() {
        System.out.print("[");
        for (int i = 0; i < mSize; i++) {
            System.out.print(mLArray[i]);
            if (i != mSize -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");
    }

    public void sort() {
        for (int i = 0; i < mSize; i++) {
            mSortedLinkedList.add(mLArray[i]);
        }

        for (int i = 0; i < mSize; i++) {
            mLArray[i] = mSortedLinkedList.remove();
        }
    }
}
