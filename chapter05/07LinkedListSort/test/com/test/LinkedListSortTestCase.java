package com.test;

import com.fireway.LinkedListSort;

import junit.framework.TestCase;

/**
 * 链表插入排序(List Insertion Sort)
 * 2018年 05月 06日 星期日
 *
 * @author fireway
 */
public class LinkedListSortTestCase extends TestCase {
    public void test() {
        LinkedListSort arr = new LinkedListSort(10);
        arr.insert(77);
        arr.insert(99);
        arr.insert(44);
        arr.insert(55);
        arr.insert(22);
        arr.insert(88);
        arr.insert(11);
        arr.insert(00);
        arr.insert(66);
        arr.insert(33);

        arr.display();

        arr.sort();

        arr.display();
    }
}
