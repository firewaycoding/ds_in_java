package com.test;

import junit.framework.TestCase;

import com.fireway.SingleLinkedList;

/**
 * 单链表V2
 * 相对于V1增加了如下功能：
 *   查找和删除指定的结点
 *
 * 2018年 04月 28日 星期六
 * @author fireway
 */
public class SingleLinkedListTestCase extends TestCase {
    public void test1() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");

        System.out.println(list);
        int index = list.indexOf("Cyan");
        System.out.println("index = " + index);

        String result = list.get(4);
        System.out.println("result = " + result);

        result = list.get(list.indexOf("Blue"));
        System.out.println("result = " + result);
    }


    public void test2() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();

        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst(null);

        System.out.println(list);
        int index = list.indexOf(null);
        System.out.println("index = " + index);
    }

    public void test3() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");

        // String result = list.get(0);
        // String result = list.get(6);
        String result = list.get(7);
        System.out.println("result = " + result);
    }

    public void test4() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");

        int result = list.indexOf("White");
        System.out.println("result = " + result);
    }

    public void test5() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");
        System.out.println(list);
        // boolean result = list.remove("Red");
        boolean result = list.remove("Purple");
        System.out.println(list);
        System.out.println("result = " + result);
    }

    public void test6() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();
        list.addFirst("Red");
        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");
        System.out.println(list);
        boolean result = list.remove("Green2");
        System.out.println(list);
        System.out.println("result = " + result);
    }

    public void test7() {
        SingleLinkedList<String> list = new SingleLinkedList<String>();

        list.addFirst("Orange");
        list.addFirst("Yellow");
        list.addFirst("Green");
        list.addFirst("Cyan");
        list.addFirst("Blue");
        list.addFirst("Purple");
        list.addFirst(null);

        System.out.println(list);
        boolean result = list.remove(null);
        System.out.println(list);
        System.out.println("result = " + result);
    }
}

