import java.util.Arrays;

import org.junit.Test;

import junit.framework.TestCase;

public class HashChainTableTestCase extends TestCase {
    @Test
    public void test() {
        int size = 10;
        HashChainTable hashChainTable = new HashChainTable(size);

        int n = 45;
        Node dataItem = null;

        int[] datas = RandomNum.read(n);
        System.out.print(Arrays.toString(datas));
        System.out.print("\n\n");

        for (int i = 0; i < n; i++) {
            dataItem = new Node(datas[i]);
            hashChainTable.insert(dataItem);
        }

        System.out.println(hashChainTable);

        int nFind = datas[8];
        Node result = hashChainTable.find(nFind);
        if (null == result) {
            System.out.println("not found!");
        } else {
            System.out.println("has found, " + nFind);
        }
    }

    /**
     * 插入第1个元素的场景
     */
    @Test
    public void testInsertCase1() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);

        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);
    }

    /**
     * 插入第2个元素，比第1个元素大
     */
    @Test
    public void testInsertCase2() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);
    }

    /**
     * 插入第2个元素，比第1个元素小
     */
    @Test
    public void testInsertCase3() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);
    }

    /**
     * 插入第3个元素，比第1个元素大，比第2个元素小
     */
    @Test
    public void testInsertCase4() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        dataItem = new Node(25);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);
    }

    /**
     * 插入重复元素
     */
    @Test
    public void testInsertCase5() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        System.out.println(System.identityHashCode(dataItem));
        hashChainTable.insert(dataItem);

        dataItem = new Node(19);
        System.out.println(System.identityHashCode(dataItem));
        hashChainTable.insert(dataItem);

        dataItem = new Node(19);
        System.out.println(System.identityHashCode(dataItem));
        hashChainTable.insert(dataItem);

        System.out.println("");
        System.out.println(hashChainTable);
        hashChainTable.displayAddress();
    }

    public void testFindCase1() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        dataItem = new Node(25);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);

        Node result = hashChainTable.find(25);
        if (null == result) {
            System.out.println("not found!");
        } else {
            System.out.println("has found.");
        }
    }

    /**
     * 只有1个元素，删除第1个元素
     */
    @Test
    public void testDeleteCase1() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);

        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);

        boolean res = hashChainTable.delete(19);
        System.out.println(res);

        System.out.println(hashChainTable);
    }

    public void testDelete2() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        dataItem = new Node(25);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);

        boolean res = hashChainTable.delete(19);
        System.out.println(res);

        System.out.println(hashChainTable);
    }

    public void testDelete3() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        dataItem = new Node(25);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);

        boolean res = hashChainTable.delete(34);
        System.out.println(res);

        System.out.println(hashChainTable);
    }

    public void testDelete4() {
        int size = 3;

        HashChainTable hashChainTable = new HashChainTable(size);

        Node dataItem = new Node(19);
        hashChainTable.insert(dataItem);

        dataItem = new Node(34);
        hashChainTable.insert(dataItem);

        dataItem = new Node(25);
        hashChainTable.insert(dataItem);

        System.out.println(hashChainTable);

        boolean res = hashChainTable.delete(25);
        System.out.println(res);

        System.out.println(hashChainTable);
    }
}

