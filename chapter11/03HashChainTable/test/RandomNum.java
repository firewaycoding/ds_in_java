import java.util.Random;

public class RandomNum {
    private static Random mRand = new Random(47);

    private static final int[] DATA = new int[256];

    static {
        for (int i = 0; i < DATA.length; i++) {
            DATA[i] = i * 2;
        }
    }

    private RandomNum() {
    };

    public static int[] read() {
        mRand.setSeed(System.nanoTime());
        int len = DATA.length;
        int[] ret = new int[len];

        boolean[] flag = new boolean[len];
        for (int i = 0; i < len; i++) {
            flag[i] = false;
        }

        for (int i = 0; i < len; i++) {
            ret[i] = getUnRepeatableData(flag);
        }

        return ret;
    }

    public static int[] read(int n) {
        mRand.setSeed(System.nanoTime());

        int len = DATA.length;
        boolean[] flag = new boolean[len];
        for (int i = 0; i < len; i++) {
            flag[i] = false;
        }

        if (n > len) {
            throw new IndexOutOfBoundsException("out of " + DATA.length);
        }

        int[] ret = new int[n];
        for (int i = 0; i < n; i++) {
            ret[i] = getUnRepeatableData(flag);
        }

        return ret;
    }

    private static int getUnRepeatableData(boolean[] flag) {
        int ret = 0;

        for (;;) {
            int index = mRand.nextInt(flag.length);
            if (!flag[index]) {
                ret = DATA[index];
                flag[index] = true;
                break;
            } else {
                continue;
            }
        }

        return ret;
    }
}


