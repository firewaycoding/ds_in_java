class SortedList {
    private Node mHead;
    
    public void insert(Node node) {
        int key = node.getKey();

        Node current = mHead;
        Node prev = null;

        while (current != null) {
            if (current.getKey() >= key) {
                // 先找到要插入的位置
                break;
            }
            
            prev = current;
            current = current.mNext;
        }
        
        if (null == prev) {
            mHead = node;
            node.mNext = current;
        } else {
            prev.mNext = node;
            node.mNext = current;
        }
    }
      
    public boolean delete(int key) {
        boolean ret = false;
        
        Node current = mHead;
        Node prev = null;
        
        while (current != null) {
            if (current.getKey() == key) {
                if (null == prev) {
                    mHead = current.mNext;
                } else {
                    prev.mNext = current.mNext;
                }
                ret = true;
                break;
            }
            
            prev = current;
            current = current.mNext;
        }
             
        return ret;
    }
    
    public Node find(int key) {
        Node ret = null;
        
        Node current = mHead;
        
        while (current != null) {
            if (key == current.getKey()) {
                ret = current;
                break;
            }
            current = current.mNext;
        }
        
        return ret;
    }
    
    public void displayAddress() {
        System.out.print("List (first-->last): ");
        Node current = mHead;
        
        while (current != null) {
            System.out.print(System.identityHashCode(current));
            System.out.print(" ");
            current = current.mNext;
        }
    }
    
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("List (first-->last): "); 
        Node current = mHead;
        
        while (current != null) {
            b.append(current);
            b.append(" ");
            current = current.mNext;
        }
     
        return b.toString();
    }
}

