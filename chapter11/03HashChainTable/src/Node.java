/**
 *
 * @author fireway
 *
 */
public class Node {
    private int mKey;

    Node mNext;

    public Node(int key) {
        mKey = key;
    }

    public int getKey() {
        return mKey;
    }

    @Override
    public String toString() {
        return Integer.toString(mKey);
    }
}

