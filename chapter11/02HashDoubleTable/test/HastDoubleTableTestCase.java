import java.util.Arrays;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import fireway.DataItem;
import fireway.HashDoubleTable;


public class HastDoubleTableTestCase extends TestCase {

    @Before
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void test1() {
        int size = 23;

        HashDoubleTable hashTable = new HashDoubleTable(size);

        int[] randoms = RandomNum.read(21);
        System.out.println(Arrays.toString(randoms));

        for (int key : randoms) {
            DataItem item = new DataItem(key);
            hashTable.insert(item);
        }

        System.out.println(hashTable);

        DataItem d = hashTable.find(31);
        System.out.println(d);

        d = hashTable.find(randoms[4]);
        System.out.println(d);

        d = hashTable.delete(randoms[4]);
        System.out.println(d);

        System.out.println(hashTable);
    }

    @Test
    public void test2() {
        int size = 23;

        HashDoubleTable hashTable = new HashDoubleTable(size);

        int[] randoms = new int[] {1,38,37,16,20,3,11,24,5,16,10,31,18,12,30,1,19,36,41,15,25};
        System.out.println(Arrays.toString(randoms));

        for (int key : randoms) {
            DataItem item = new DataItem(key);
            hashTable.insert(item);
        }

        System.out.println(hashTable);

        DataItem d = hashTable.find(57);
        System.out.println(d);

        d = hashTable.find(randoms[4]);
        System.out.println(d);

        d = hashTable.delete(randoms[4]);
        System.out.println(d);

        System.out.println(hashTable);
    }
}

