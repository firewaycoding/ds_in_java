package fireway;

public class DataItem {
    private int mData;

    public DataItem(int data) {
        mData = data;
    }

    public int getData() {
        return mData;
    }
}

