
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import fireway.DataItem;
import fireway.HashTable;

import junit.framework.TestCase;


public class HastTableTestCase extends TestCase {

    @Before
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * 如果想看哈希表的执行过程，最好创建一个少于20项的哈希表，为的是所有数据项都可以显示在一行中。
     */
    @Test
    public void test() {
        int size = 10;

        HashTable hashTable = new HashTable(size);

        int[] randoms = RandomNum.read(8);
        System.out.println(Arrays.toString(randoms));

        for (int key : randoms) {
            DataItem item = new DataItem(key);
            hashTable.insert(item);
        }

        hashTable.display();

        DataItem d = hashTable.find(31);
        System.out.println(d);

        d = hashTable.find(randoms[4]);
        System.out.println(d);

        d = hashTable.delete(randoms[4]);
        System.out.println(d);

        hashTable.display();
    }
}

