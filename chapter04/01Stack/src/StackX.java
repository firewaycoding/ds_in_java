/**
 * 2018年 03月 23日 星期五
 *
 * @author fireway
 */
public class StackX {
    private long mLArray[];

    private int mTop;

    public StackX(int initialCapacity) {
        mLArray = new long[initialCapacity];
        mTop = -1;
    }

    /**
     * 入栈
     * put item on top of stack
     */
    public void push(long item) {
        if (full()) {
            System.out.println("Can't insert, stack is full");
            return;
        }

        mLArray[++mTop] = item;
    }

    /**
     * 出栈
     * take item from top of stack
     */
    public long pop() {
        if (empty()) {
            System.out.println("Can't pop, stack is empty");
            return -1;
        }

        long temp = mLArray[mTop];
        mTop--;
        return temp;
    }

    /**
     * 查看
     * peek at top of stack
     */
    public long peek() {
        return mLArray[mTop];
    }

    /**
     * 是否栈空。
     * true if stack is empty
     */
    public boolean empty() {
        return (-1 == mTop);
    }

    /**
     * 是否栈满
     * true if stack is full
     */
    public boolean full() {
        return (mLArray.length - 1 == mTop);
    }
}

