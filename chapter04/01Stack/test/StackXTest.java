/**
 * 2018年 03月 23日 星期五
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class StackXTest extends TestCase {
    public void test() {
        StackX x = new StackX(10);
        x.push(20);
        x.push(40);
        x.push(60);
        x.push(80);

        while (!x.empty()) {
            long item = x.pop();
            System.out.print(item);
            System.out.print(" ");
        }
        System.out.println("");
    }

    public void test2() {
        StackX x = new StackX(3);
        x.push(20);
        x.push(40);
        x.push(60);
        x.push(80);

        while (!x.empty()) {
            long item = x.pop();
            System.out.print(item);
            System.out.print(" ");
        }
        System.out.println("");
    }

    public void test3() {
        StackX x = new StackX(3);
        x.push(20);
        x.push(40);
        x.push(60);

        while (!x.empty()) {
            long item = x.pop();
            System.out.print(item);
            System.out.print(" ");
        }
        System.out.println("");
        x.pop();
        System.out.println("");
    }
}

