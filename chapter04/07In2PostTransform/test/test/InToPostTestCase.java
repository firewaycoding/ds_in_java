package test;

import junit.framework.TestCase;
import fireway.In2PostTransform;
import fireway.InToPost;

/**
 * <p> 中缀表达式转换成后缀表达式 </p>
 *
 * <p> 2018年 04月 22日 星期日 </p>
 *
 * @author fireway
 */
public class InToPostTestCase extends TestCase {
    public void test1() {
        String infix = "A+B*(C-D)";
        System.out.println(infix);
        In2PostTransform in2PostTransForm = new In2PostTransform(infix);
        String postfix = in2PostTransForm.transform();
        System.out.println(postfix);
        String expert = "ABCD-*+";
        assertTrue(expert.equals(postfix));
    }

    public void test2() {
        String infix = "A+B-C+D-E";
        System.out.println(infix);
        In2PostTransform in2PostTransForm = new In2PostTransform(infix);
        String postfix = in2PostTransForm.transform();
        System.out.println(postfix);
        String expert = "AB+C-D+E-";
        assertTrue(expert.equals(postfix));
    }

    public void test3() {
        String infix = "A+B*(C-D/(E+F))";
        System.out.println(infix);
        In2PostTransform in2PostTransForm = new In2PostTransform(infix);
        String postfix = in2PostTransForm.transform();
        System.out.println(postfix);
        String expert = "ABCDEF+/-*+";
        assertTrue(expert.equals(postfix));
    }

    public void test4() {
        String infix = "B/C*D-A";
        System.out.println(infix);
        In2PostTransform in2PostTransForm = new In2PostTransform(infix);
        String postfix = in2PostTransForm.transform();
        System.out.println(postfix);
    }
}
