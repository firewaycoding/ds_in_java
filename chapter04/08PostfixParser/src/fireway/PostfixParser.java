package fireway;

/**
 * 后缀表达式求值
 * 2018年 04月 23日 星期一
 *
 * @author fireway
 */
public class PostfixParser {
    private StackX mStacX;

    private String mInput;

    public PostfixParser(String input) {
        mStacX = new StackX(input.length());
        mInput = input;
    }

    public int parse() {
        for (int i = 0; i < mInput.length(); i++) {
            char current = mInput.charAt(i);
            System.out.println("For " + current + " " + mStacX.toString());
            if (current >= '0' && current <= '9') {
                int number = (int)(current - '0');
                mStacX.push(number);
            } else {
                int num2 = mStacX.pop();
                int num1 = mStacX.pop();
                int interResult = 0;
                switch(current) {
                    case '+':
                        interResult = num1 + num2;
                        break;

                    case '-':
                        interResult = num1 - num2;
                        break;

                    case '*':
                        interResult = num1 * num2;
                        break;

                    case '/':
                        interResult = num1 / num2;
                        break;

                    default:

                        break;
                }
                mStacX.push(interResult);
            }
        }

        return mStacX.pop();
    }

    class StackX {
        private int mIntArray[];

        private int mTop;

        public StackX(int initialCapacity) {
            mIntArray = new int[initialCapacity];
            mTop = -1;
        }

        /**
         * 入栈
         * put item on top of stack
         */
        public void push(int item) {
            if (full()) {
                System.out.println("Can't insert, stack is full");
                return;
            }

            mIntArray[++mTop] = item;
        }

        /**
         * 出栈
         * take item from top of stack
         */
        public int pop() {
            if (empty()) {
                System.out.println("Can't pop, stack is empty");
                return Character.MIN_VALUE;
            }

            int temp = mIntArray[mTop];
            mTop--;
            return temp;
        }

        /**
         * 查看
         * peek at top of stack
         */
        public int peek() {
            return mIntArray[mTop];
        }

        /**
         * 是否栈空。
         * true if stack is empty
         */
        public boolean empty() {
            return (-1 == mTop);
        }

        /**
         * 是否栈满
         * true if stack is full
         */
        public boolean full() {
            return (mIntArray.length - 1 == mTop);
        }

        public int size() {
            return mTop + 1;
        }

        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append("[");
            int n = size();
            for (int i = 0; i < n; i++) {
                sb.append(mIntArray[i]);
                if (i != n - 1) {
                    sb.append(" ");
                }
            }
            sb.append("]");
            return sb.toString();
        }
    }
}
