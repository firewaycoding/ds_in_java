package test;

import junit.framework.TestCase;
import fireway.PostfixParser;

/**
 * 后缀表达式求值
 * 2018年 04月 23日 星期一
 *
 * @author fireway
 */
public class PostfixParserTestCase extends TestCase {
    public void test1() {
        // 3+4*(9-2)
        String input = "3492-*+";
        PostfixParser postfixParser = new PostfixParser(input);
        int result = postfixParser.parse();
        System.out.println(result);
        int expert = 31;
        assertEquals(expert, result);
    }

    public void test2() {
        // 3*(4+5)-6/(1+2)
        String input = "345+*612+/-";
        PostfixParser postfixParser = new PostfixParser(input);
        int result = postfixParser.parse();
        System.out.println(result);
        int expert = 25;
        assertEquals(expert, result);
    }
}
