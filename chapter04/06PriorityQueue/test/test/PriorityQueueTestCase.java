package test;

import fireway.PriorityQueue;
import junit.framework.TestCase;

/**
 * 优先级队列
 * 2018年 04月 11日 星期三
 */
public class PriorityQueueTestCase extends TestCase {
    public void test() {
        PriorityQueue pq = new PriorityQueue(5);
        pq.add(30);
        pq.add(50);
        pq.add(10);
        pq.add(40);
        pq.add(20);

        while (!pq.isEmpty()) {
            long e = pq.remove();
            System.out.print(e);
            System.out.print(" ");
        }
        System.out.println("");
    }
}
