package test;

import fireway.Queue;
import junit.framework.TestCase;

/**
 * 循环队列测试
 * 2018年 04月 05日 星期四
 *
 * @author fireway
 */
public class QueueTest extends TestCase {
    public void test1() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);
        System.out.println("size = " + q.size());
        q.remove();
        q.remove();
        q.remove();
        System.out.println("size = " + q.size());
        q.add(50);
        q.add(60);
        q.add(70);
        q.add(80);
        System.out.println("size = " + q.size());

        while (!q.isEmpty()) {
            long e = q.remove();
            System.out.print(e);
            System.out.print(" ");
        }
        System.out.println("");
    }

    /**
     * 测试队列满的情况
     */
    public void test2() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);
        q.add(50);
        q.add(60);
        q.add(70);
    }

    public void test3() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);
        q.add(50);
        q.remove();
        q.remove();
        q.remove();
        q.add(60);
        q.add(70);
        q.add(80);
        q.add(90);
    }

    /**
     * 测试队列空的情况
     */
    public void test4() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);
        q.add(50);
        System.out.println("size = " + q.size());
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
    }

    public void test5() {
        Queue q = new Queue(5);
        System.out.println("size = " + q.size());
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
    }
}
