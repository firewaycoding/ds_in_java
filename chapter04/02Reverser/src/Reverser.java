/**
 * 2018年 03月 24日 星期六
 *
 * @author fireway
 */
public class Reverser {
    public String mInPut;

    public String mOutPut;

    public Reverser(String input) {
        mInPut = input;
    }

    public String reverse() {
        int size = mInPut.length();
        StackX x = new StackX(size);
        for (int i = 0; i < size; i++) {
            char c = mInPut.charAt(i);
            x.push(c);
        }

        mOutPut = "";
        while(!x.empty()) {
            char c = x.pop();
            mOutPut += c;

        }

        return mOutPut;
    }

    class StackX {
        private char[] mCArray;

        private int mTop;

        public StackX(int initialCapacity) {
            mCArray = new char[initialCapacity];
            mTop = -1;
        }

        public void push(char item) {
            if (full()) {
                System.out.println("Can't insert, stack is full");
                return;
            }

            mCArray[++mTop] = item;
        }

        public char pop() {
            if (empty()) {
                System.out.println("Can't pop, stack is empty");
                return 0;
            }

            char temp = mCArray[mTop];
            mTop--;
            return temp;
        }

        public char peek() {
            return mCArray[mTop];
        }

        private boolean empty() {
            return (-1 == mTop);
        }

        private boolean full() {
            return (mCArray.length - 1 == mTop);
        }
    }
}
