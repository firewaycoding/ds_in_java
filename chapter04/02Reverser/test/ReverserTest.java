/**
 * 2018年 03月 24日 星期六
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class ReverserTest extends TestCase {
    public void test() {
        String input = "we are the world!";
        System.out.println(input);
        Reverser rev = new Reverser(input);
        String output = rev.reverse();
        System.out.println(output);

        input = "Now we are learing Data Structures and Algorithms in Java(2nd Edition)";
        System.out.println(input);
        rev = new Reverser(input);
        output = rev.reverse();
        System.out.println(output);
    }
}
