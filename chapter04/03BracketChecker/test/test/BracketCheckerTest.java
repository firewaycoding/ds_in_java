package test;

import fireway.BracketChecker;
import junit.framework.TestCase;

/**
 * 分隔符匹配
 * 2018年 03月 25日 星期日
 *
 * @author fireway
 */
public class BracketCheckerTest extends TestCase {
    public void test1() {
        String input = "c[d]";
        System.out.println(input);

        boolean res = false;
        try {
            res = BracketChecker.check(input);
        } catch (BracketChecker.MismatchException e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }

    public void test2() {
        String input = "a{b[c]d}e";
        System.out.println(input);

        boolean res = false;
        try {
            res = BracketChecker.check(input);
        } catch (BracketChecker.MismatchException e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }

    /**
     * error1
     */
    public void test3() {
        String input = "a{b(c]d}e";
        System.out.println(input);

        boolean res = false;
        try {
            res = BracketChecker.check(input);
        } catch (BracketChecker.MismatchException e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }

    /**
     * error2
     */
    public void test4() {
        String input = "a[b{c}d]e}";
        System.out.println(input);

        boolean res = false;
        try {
            res = BracketChecker.check(input);
        } catch (BracketChecker.MismatchException e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }

    /**
     * error3
     */
    public void test5() {
        String input = "a{b(c)";
        System.out.println(input);

        boolean res = false;
        try {
            res = BracketChecker.check(input);
        } catch (BracketChecker.MismatchException e) {
            e.printStackTrace();
        }
        System.out.println(res);
    }
}
