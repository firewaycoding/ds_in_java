package fireway;

import java.util.NoSuchElementException;

/**
 * 循环队列
 * 2018年 03月 29日
 *
 * @author fireway
 */
public class Queue {
    // Number of items in the queue
    private int mSize;

    // The queued items
    private long[] mItems;

    // 队头
    private int mFront;

    // 队尾
    private int mRear;

    private static final long NULL_Element = 0L;

    public Queue(int initialCapacity) {
        mItems = new long[initialCapacity];
        mSize = 0;
        mFront = 0;
        mRear = -1;
    }

    public boolean add(long e) {
        if (isFull()) {
            throw new IllegalStateException("Queue full: rear = " + mRear
                                            + ", front =" + mFront);
        } else {
            insert(e);
            return true;
        }
    }

    public boolean offer(long e) {
        if (isFull()) {
            System.out.println("Queue full: rear = " + mRear + ", front ="
                               + mFront);
            return false;
        } else {
            insert(e);
            return true;
        }
    }

    public long remove() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue empty: rear = " + mRear
                                             + ", front =" + mFront);
        } else {
            return extract();
        }
    }

    public long poll() {
        if (isEmpty()) {
            System.out.println("Queue empty: rear = " + mRear + ", front ="
                               + mFront);
            return NULL_Element;
        } else {
            return extract();
        }
    }

    /**
     * 获取但不移除此队列的头
     */
    public long peek() {
        if (isEmpty()) {
            return NULL_Element;
        } else {
            return mItems[mFront];
        }
    }

    public boolean isEmpty() {
        return 0 == mSize;
    }

    public boolean isFull() {
        return mItems.length == mSize;
    }

    public int size() {
        return mSize;
    }

    private void insert(long e) {
        circularlyReset();

        mItems[++mRear] = e;
        mSize++;
    }

    private long extract() {
        circularlyReset();

        long tmp = mItems[mFront];
        mItems[mFront] = NULL_Element;
        mFront++;
        mSize--;
        return tmp;
    }

    private void circularlyReset() {
        if (mItems.length - 1 == mRear) {
            mRear = -1;
        }

        if (mItems.length == mFront) {
            mFront = 0;
        }
    }
}

