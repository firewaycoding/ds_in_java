package test;

import fireway.Queue;
import junit.framework.TestCase;

/**
 * 循环队列测试
 * 2018年 03月 29日
 *
 * @author fireway
 */
public class QueueTest extends TestCase {
    public void test1() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);

        q.remove();
        q.remove();
        q.remove();

        q.add(50);
        q.add(60);
        q.add(70);
        q.add(80);

        while (!q.isEmpty()) {
            long e = q.remove();
            System.out.print(e);
            System.out.print(" ");
        }
        System.out.println("");
    }

    public void test2() {
        Queue q = new Queue(5);
        q.offer(10);
        q.offer(20);
        q.offer(30);
        q.offer(40);
        q.offer(50);
        q.offer(60);
    }

    public void test3() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);
        q.add(50);
        q.add(60);
    }


    public void test4() {
        Queue q = new Queue(5);
        q.offer(10);
        q.offer(20);
        q.offer(30);
        q.offer(40);

        q.poll();
        q.poll();
        q.poll();
        q.poll();
        q.poll();
        q.poll();
    }

    public void test5() {
        Queue q = new Queue(5);
        q.add(10);
        q.add(20);
        q.add(30);
        q.add(40);

        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
        q.remove();
    }
}

