/**
 * long数组封装类
 * 2018年 03月 08日 星期四
 *
 * @author fireway
 */
public class LowArray {
    private long[] mLArray;

    public LowArray(int size) {
        mLArray = new long[size];
    }

    public void setElement(int index, long value) {
        mLArray[index] = value;
    }

    public long getElement(int index) {
        return mLArray[index];
    }
}

