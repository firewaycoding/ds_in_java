/**
 * 2018年 03月 08日 星期四
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class LowArrayTest extends TestCase {
    public void test() {
        LowArray lowArray = new LowArray(10);
        int size = 0;
        int i = 0;

        lowArray.setElement(0, 77);
        lowArray.setElement(1, 99);
        lowArray.setElement(2, 44);
        lowArray.setElement(3, 55);
        lowArray.setElement(4, 22);
        lowArray.setElement(5, 88);
        lowArray.setElement(6, 11);
        lowArray.setElement(7, 0);
        lowArray.setElement(8, 66);
        lowArray.setElement(9, 33);
        size = 10;

        // 显示
        System.out.print("[");
        for(i = 0; i <size; i++) {
            System.out.print(lowArray.getElement(i));
            if (i != size -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println("");

        // 查找
        int searchKey = 66;
        for(i = 0; i <size; i++) {
            if (searchKey == lowArray.getElement(i)) {
                break;
            }
        }
        if (i == size) {
            System.out.println("Can't find " + searchKey);
        } else {
            System.out.println("Found " + searchKey);
        }

        // 边界测试：删除第一个元素
        searchKey = 77;
        for(i = 0; i <size; i++) {
            if (searchKey == lowArray.getElement(i)) {
                break;
            }
        }
        // 注意这里j应该小于size - 1
        for (int j = i; j < size - 1; j++) {
            lowArray.setElement(j, lowArray.getElement(j + 1));
        }
        size--;

        // 显示
        System.out.print("[");
        for(i = 0; i <size; i++) {
            System.out.print(lowArray.getElement(i));
            if (i != size -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println("");
    }
}

