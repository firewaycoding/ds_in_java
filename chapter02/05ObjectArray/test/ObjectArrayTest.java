/**
 * 2018年 03月 10日 星期六
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class ObjectArrayTest extends TestCase {
    public void test1() {
        ObjectArray<Person> persons = new ObjectArray<Person>(10);

        Person[] ps = {
            new Person("Evans", "Patty", 24),
            null,
            new Person("Yee", "Tom", 43),
            new Person("Adams", "Henry", 63),
            new Person("Hashimoto", "Sato", 21),
            new Person("Stimson", "Henry", 29),
            new Person("Velasquez", "Jose", 72),
            new Person("Lamarque", "Henry", 54),
            new Person("Vang", "Minh", 22),
            new Person("Creswell", "Lucinda", 18),
            new Person("Fireway", "Wei", 18)
        };

        for (Person p : ps) {
            persons.insert(p);
        }

        System.out.println("size = " + persons.size());
        System.out.println(persons.toString());

        Person p1 = new Person("Hashimoto", "Sato");
        int index = persons.indexOf(p1);
        System.out.println("index = " + index);

        Person p2 = new Person("Evans", "Patty");
        Person p3 = new Person("Fireway", "Wei");
        Person p4 = new Person("Creswell", "Lucinda");
        persons.delete(p2);
        persons.delete(p3);
        persons.delete(p4);
        System.out.println("size = " + persons.size());
        System.out.println(persons.toString());
    }


    public void test2() {
        ObjectArray<Integer> numbers = new ObjectArray<Integer>();
        int[] nums = {5, 7, 1, 3, 6, 4, 8, 2, 9, 15, 17, 18};
        for (int n : nums) {
            numbers.insert(n);
        }
        System.out.println("size = " + numbers.size());
        System.out.println(numbers.toString());

        for (int n : nums) {
            boolean b = numbers.delete(n);
            System.out.println("delete " + n + " " + b);
        }
        System.out.println("size = " + numbers.size());
        System.out.println(numbers.toString());
    }

    /**
     * 边界测试： 测试删除数组第一个元素
     */
    public void test3() {
        ObjectArray<Integer> numbers = new ObjectArray<Integer>();
        int[] nums = {5, 7, 1, 3, 6, 4, 8, 2, 9, 15};
        for (int n : nums) {
            numbers.insert(n);
        }
        System.out.println("size = " + numbers.size());
        System.out.println(numbers.toString());


        boolean b = numbers.delete(5);
        System.out.println("delete " + b);
        System.out.println("size = " + numbers.size());
        System.out.println(numbers.toString());
    }

    /**
     * 边界测试： 测试删除数组最后一个元素
     */
    public void test4() {
        ObjectArray<Integer> numbers = new ObjectArray<Integer>();
        int[] nums = {5, 7, 1, 3, 6, 4, 8, 2, 9, 15};
        for (int n : nums) {
            numbers.insert(n);
        }
        System.out.println("size = " + numbers.size());
        System.out.println(numbers.toString());


        boolean b = numbers.delete(15);
        System.out.println("delete " + b);
        System.out.println("size = " + numbers.size());
        System.out.println(numbers.toString());
    }
}

