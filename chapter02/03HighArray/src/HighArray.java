/**
 * 2018年 03月 08日 星期四
 * @author fireway
 */
public class HighArray {
    private long[] mLArray;
    private int mSize;

    public HighArray(int n) {
        mLArray = new long[n];
        mSize = 0;
    }

    public boolean find(long key) {
        boolean ret = false;

        int i = 0;
        for (i = 0; i < mSize; i++) {
            if (key == mLArray[i]) {
                break;
            }
        }
        if (i == mSize) {
            ret = false;
        } else {
            ret = true;
        }

        return ret;
    }

    public void add(long value) {
        mLArray[mSize] = value;
        mSize++;
    }

    public boolean delete(long value) {
        boolean ret = false;

        int i = 0;
        for (i = 0; i < mSize; i++) {
            if (value == mLArray[i]) {
                break;
            }
        }
        if (i == mSize) {
            ret = false;
        } else {
            // 注意：这里的j应该小于mSize - 1
            for (int j = i; j < mSize - 1; j++) {
                mLArray[j] = mLArray[j + 1];
            }
            mSize--;
            ret = true;
        }

        return ret;
    }

    public void display() {
        System.out.print("[");
        for (int i = 0; i < mSize; i++) {
            System.out.print(mLArray[i]);
            if (i != mSize -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");
    }
}

