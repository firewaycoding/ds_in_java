/**
 * 2018年 03月 08日 星期四
 * @author fireway
 */
import junit.framework.TestCase;

public class HighArrayTest extends TestCase {
    public void test() {
        HighArray highArray = new HighArray(100);
        highArray.add(77);
        highArray.add(99);
        highArray.add(44);
        highArray.add(55);
        highArray.add(22);
        highArray.add(88);
        highArray.add(11);
        highArray.add(00);
        highArray.add(66);
        highArray.add(33);

        highArray.display();

        long[] keys = { 98, 0, 66, 57 };
        for (long k : keys) {
            if (highArray.find(k)) {
                System.out.println("Found " + k);
            } else {
                System.out.println("Can't find " + k);
            }
        }

        long[] values = {77, 99, 44, 55, 22, 88, 11, 00, 66, 33, 5};
        for (long value : values) {
            if (highArray.delete(value)) {
                System.out.println("delete " + value + " ok");
            } else {
                System.out.println("delete " + value + " fail");
            }
        }

        highArray.display();
    }

    /**
     * 边界测试
     */
    public void test2() {
        HighArray highArray = new HighArray(10);
        highArray.add(77);
        highArray.add(99);
        highArray.add(44);
        highArray.add(55);
        highArray.add(22);
        highArray.add(88);
        highArray.add(11);
        highArray.add(00);
        highArray.add(66);
        highArray.add(33);

        highArray.display();

        boolean b = highArray.find(77);
        System.out.println("find " + b);

        b = highArray.delete(77);
        System.out.println("delete " + b);

        highArray.display();
    }

    /**
     * 边界测试
     */
    public void test3() {
        HighArray highArray = new HighArray(10);
        highArray.add(77);
        highArray.add(99);
        highArray.add(44);
        highArray.add(55);
        highArray.add(22);
        highArray.add(88);
        highArray.add(11);
        highArray.add(00);
        highArray.add(66);
        highArray.add(33);

        highArray.display();

        boolean b = highArray.find(33);
        System.out.println("find " + b);

        b = highArray.delete(33);
        System.out.println("delete " + b);

        highArray.display();
    }
}

