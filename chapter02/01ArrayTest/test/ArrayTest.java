import junit.framework.TestCase;

public class ArrayTest extends TestCase {
    public void test() {
        long[] arr = new long[10];
        int size = 0;
        int i = 0;
        long searchKey = 0L;

        arr[0] = 77;
        arr[1] = 99;
        arr[2] = 44;
        arr[3] = 55;
        arr[4] = 22;
        arr[5] = 88;
        arr[6] = 11;
        arr[7] = 00;
        arr[8] = 66;
        arr[9] = 33;
        size = 10;

        // 显示
        System.out.print("[");
        for(i = 0; i <size; i++) {
            System.out.print(arr[i]);
            if (i != size -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println("");

        // 看一下数组名和第一个元素的地址是否相同
        System.out.println(System.identityHashCode(arr));
        System.out.println(System.identityHashCode(arr[0]));
        System.out.println(System.identityHashCode(arr[1]));

        // 边界测试：查找第一个1元素
        searchKey = 77;
        for(i = 0; i <size; i++) {
            if (searchKey == arr[i]) {
                break;
            }
        }
        if (i == size) {
            System.out.println("Can't find " + searchKey);
        } else {
            System.out.println("Found " + searchKey);
        }

        // 删除第一个元素
        searchKey = 77;
        for(i = 0; i <size; i++) {
            if (searchKey == arr[i]) {
                break;
            }
        }
        // 注意这里的j应该小于size - 1
        for (int j = i; j < size - 1; j++) {
            arr[j] = arr[j + 1];
        }
        size--;

        System.out.print("[");
        for(i = 0; i <size; i++) {
            System.out.print(arr[i]);
            if (i != size -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println("");
    }
}

