import java.util.Arrays;

import junit.framework.TestCase;

public class Tree234TestCase extends TestCase {
    public void test1() {
        Tree234 tree = new Tree234();

        long[] datas = RandomNum.read(32);
        System.out.println(Arrays.toString(datas));

        for (int i = 0; i < 32; i++) {
            tree.insert(datas[i]);
        }

        tree.displayTree();
    }

    public void test2() {
        Tree234 tree = new Tree234();

        long[] datas = new long[] {
            194, 210, 130, 142,
            226, 34, 74, 178,
            46, 38, 170, 98,
            218, 278, 70, 118,
            146, 174, 114, 106,
            50, 158, 282, 138,
            90, 266, 274, 78,
            86, 94, 214, 242
        };

        System.out.println(Arrays.toString(datas));

        for (int i = 0; i < 32; i++) {
            tree.insert(datas[i]);
        }

        tree.displayTree();
    }

    public void testRead() {
        for (int i = 0; i < 20; i++) {
            long[] datas = RandomNum.read();
            System.out.println(Arrays.toString(datas));
        }
    }
}
