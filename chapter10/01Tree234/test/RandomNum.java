import java.util.Random;

public class RandomNum {
    private static Random mRand = new Random(47);

    private static final long[] DATA = new long[256];

    static {
        for (int i = 0; i < DATA.length; i++) {
            DATA[i] = 30 + i * 4;
        }
    }

    private RandomNum() {
    };

    public static long[] read() {
        mRand.setSeed(System.nanoTime());
        int len = DATA.length;
        long[] ret = new long[len];

        boolean[] flag = new boolean[len];
        for (int i = 0; i < len; i++) {
            flag[i] = false;
        }

        for (int i = 0; i < len; i++) {
            ret[i] = getUnRepeatableData(flag);
        }

        return ret;
    }

    public static long[] read(int n) {
        mRand.setSeed(System.nanoTime());

        int len = DATA.length;
        boolean[] flag = new boolean[len];
        for (int i = 0; i < len; i++) {
            flag[i] = false;
        }

        if (n > len) {
            throw new IndexOutOfBoundsException("out of " + DATA.length);
        }

        long[] ret = new long[n];
        for (int i = 0; i < n; i++) {
            ret[i] = getUnRepeatableData(flag);
        }

        return ret;
    }

    private static long getUnRepeatableData(boolean[] flag) {
        long ret = 0;

        for (;;) {
            int index = mRand.nextInt(flag.length);
            if (!flag[index]) {
                ret = DATA[index];
                flag[index] = true;
                break;
            } else {
                continue;
            }
        }

        return ret;
    }
}

