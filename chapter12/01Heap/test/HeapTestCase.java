/**
 * 堆
 * 2018年 12月 01日 星期六
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class HeapTestCase extends TestCase {
    public void testInsertAndRemove() {
        Heap heap = new Heap(31);
        heap.insert(70);
        heap.insert(40);
        heap.insert(50);
        heap.insert(20);
        heap.insert(60);
        heap.insert(100);
        heap.insert(80);
        heap.insert(30);
        heap.insert(10);
        heap.insert(90);
        heap.display();
        heap.remove();
        heap.display();
        heap.remove();
        heap.display();
    }

    public void testRemoveOnlyOne() {
        Heap heap = new Heap(1);
        heap.insert(70);
        heap.display();
        heap.remove();
        heap.display();
        heap.insert(60);
        heap.display();
    }

    public void testChange() {
        Heap heap = new Heap(31);
        heap.insert(70);
        heap.insert(40);
        heap.insert(50);
        heap.insert(20);
        heap.insert(60);
        heap.insert(100);
        heap.insert(80);
        heap.insert(30);
        heap.insert(10);
        heap.insert(90);
        heap.display();
        heap.change(5, 151);
        heap.display();
    }

    public void testHeapFull() {
        Heap heap = new Heap(10);
        heap.insert(70);
        heap.insert(40);
        heap.insert(50);
        heap.insert(20);
        heap.insert(60);
        heap.insert(100);
        heap.insert(80);
        heap.insert(30);
        heap.insert(10);
        heap.insert(90);
        heap.insert(151);
        heap.display();
    }

    public void testHeapEmpty() {
        Heap heap = new Heap(10);
        heap.insert(70);
        heap.insert(40);
        heap.insert(50);
        heap.insert(20);
        heap.display();
        heap.remove();
        heap.remove();
        heap.remove();
        heap.remove();
        heap.remove();
        heap.remove();
        heap.display();
    }

    public void testHeapSort() {
        int[] test = {18, 21, 3, 6, 2, 4, 50, 88, 37, 65};
        Heap heap = new Heap(31);
        for (int i = 0; i < test.length; i++) {
            heap.insert(test[i]);
        }
        heap.display();

        while(!heap.isEmpty()) {
            System.out.print(heap.remove().getData() + " ");
        }
    }

    public void testHeapSort2() {
        int[] test = {00, 11, 22, 33, 44, 55, 66, 77, 88, 99};
        Heap heap = new Heap(31);
        for (int i = 0; i < test.length; i++) {
            heap.insert(test[i]);
        }
        heap.display();

        while(!heap.isEmpty()) {
            System.out.print(heap.remove().getData() + " ");
        }
    }
}
