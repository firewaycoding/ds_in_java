/**
 * 堆排序
 * 2018年 12月 04日 星期二
 *
 * @author fireway
 */
class Node {
    private int mData;

    public Node(int data) {
        mData = data;
    }

    public void setData(int data) {
        mData = data;
    }

    public int getData() {
        return mData;
    }

    @Override
    public String toString() {
        return "{" + mData + "}";
    }
}

public class Heap {
    private Node[] mArray;

    private int mSize;

    public Heap(int initialCapacity) {
        mArray = new Node[initialCapacity];
        mSize = 0;
    }


    public boolean isFull() {
        return mArray.length == mSize;
    }

    public boolean isEmpty() {
        return 0 == mSize;
    }

    public void displayHeap() {
        String dots = "...............................";
        System.out.println(dots+dots);
        int column = 0;
        int nBlanks = 32;
        int nItemsPerRow = 1;
        for (int i = 0; i < mSize; i++) {
            if (column == 0) {
                for (int j = 0; j < nBlanks; j++) {
                    System.out.print(' ');
                }
            }
            System.out.print(mArray[i].getData());

            if (++column == nItemsPerRow) {
                nBlanks /= 2;
                nItemsPerRow *= 2;
                column = 0;
                System.out.println();
            } else {
                for (int j = 0; j < nBlanks * 2 - 2; j++) {
                    System.out.print(' ');
                }
            }
        }
        System.out.println("\n"+dots+dots);
    }

    public void displayArray() {
        for (int i = 0; i < mArray.length; i++) {
            if (mArray[i] != null) {
                System.out.print(mArray[i].getData() + " ");
            } else {
                System.out.print("-- ");
            }
        }
        System.out.println();
    }

    public Node remove() {
        if (isEmpty()) {
            System.out.println("Heap empty!");
            return null;
        }
        Node root = mArray[0];
        mArray[0] = mArray[mSize - 1];
        mArray[mSize - 1] = null;
        mSize--;
        trickleDown(0);
        return root;
    }

    public void trickleDown(int currentIndex) {
        Node tmp = mArray[currentIndex];

        int index = currentIndex;
        // while ((index * 2 + 1) <= (mSize - 1)) {
        while (index < (mSize - 1) / 2) {
            int leftChild = index * 2 + 1;
            int rightChild = leftChild + 1;

            int largerChild = 0;
            if (mArray[rightChild] == null || (mArray[leftChild].getData() > mArray[rightChild].getData())) {
                largerChild = leftChild;
            } else {
                largerChild = rightChild;
            }

            if (tmp.getData() >= mArray[largerChild].getData()) {
                break;
            }
            mArray[index] = mArray[largerChild];
            index = largerChild;
        }

        mArray[index] = tmp;
    }

    public void insertAt(int index, Node node) {
        if (index > mArray.length - 1) {
            System.out.println("insertAt index error: " + index);
            return;
        }
        mArray[index] = node;
        // 注意不能放在这里自增，因为remove()和insertAt在同一个循环体内执行
        // !mSize++;
    }

    public void incrementSize() {
        mSize++;
    }
}
