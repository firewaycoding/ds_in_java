/**
 * 堆排序
 * 2018年 12月 04日 星期二
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class HeapTestCase extends TestCase {
    public void test() {
        int size = 31;
        Heap heap = new Heap(size);

        for (int i = 0; i < size; i++) {
            int random = (int)(Math.random() * 100);
            Node node = new Node(random) ;
            heap.insertAt(i, node);
            heap.incrementSize();
        }

        System.out.print("Random: ");
        heap.displayArray();

        for (int i = size / 2 - 1; i >=0; i--) {
            heap.trickleDown(i);
        }

        System.out.print("Heap:   ");
        heap.displayArray();
        heap.displayHeap();

        for (int i = size - 1; i >= 0; i--) {
            Node biggest = heap.remove();
            heap.insertAt(i, biggest);
        }
        System.out.print("Sorted: ");
        heap.displayArray();
    }
}
