package fireway;

/**
 * 快速排序算法 - 三数据项取中
 * 2018年 08月 02日 星期四
 *
 * @author fireway
 */
public class QuickSort {
    private long mLArray[];

    private int mSize;

    public QuickSort(int initialCapacity) {
        mLArray = new long[initialCapacity];
        mSize = 0;
    }

    public void insert(long e) {
        mLArray[mSize++] = e;
    }

    public void display() {
        System.out.print("[");
        for (int i = 0; i < mSize; i++) {
            System.out.print(mLArray[i]);
            if (i != mSize - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");
    }

    private void swap(int left, int right) {
        long temp = mLArray[left];
        mLArray[left] = mLArray[right];
        mLArray[right] = temp;
    }

    public void quickSort() {
        recQuickSort(0, mSize - 1);
    }

    private void recQuickSort(int left, int right) {
        int size = right - left + 1;
        if (size <= 3) {
            manualSort(left, right);
        } else {
            long median = medianOfThreeItem(left, right);
            int partitionIndex = partitioning(left, right, median);
            recQuickSort(left, partitionIndex - 1);
            recQuickSort(partitionIndex + 1, right);
        }
    }

    /**
     * 中值划分的要求至少含有4个数据项
     */
    private long medianOfThreeItem(int left, int right) {
        int middle = (left + right) / 2;
        // left center
        if (mLArray[left] > mLArray[middle]) {
            swap(left, middle);
        }
        // left right
        if (mLArray[left] > mLArray[right]) {
            swap(left, right);
        }
        // center right
        if (mLArray[middle] > mLArray[right]) {
            swap(middle, right);
        }

        swap(middle, right - 1);

        return mLArray[right - 1];
    }

    private int partitioning(int left, int right, long pivot) {
        int leftPointer = left;
        int rightPointer = right - 1;

        while(true) {
            while(mLArray[++leftPointer] < pivot) {}

            // 三数据项取中方法除了选择枢纽更为有效之前，还有一个好处：
            // 可以在第二个内部while循环中取消rightPointer > left的检测，
            // 以略微提高算法的执行速度。
            while(mLArray[--rightPointer] > pivot) {}

            if (leftPointer >= rightPointer) {
                break;
            } else {
                swap(leftPointer, rightPointer);
            }
        }
        swap(leftPointer, right - 1);

        return leftPointer;
    }

    private void manualSort(int left, int right) {
        int size = right - left + 1;
        switch (size) {
            case 1: {
                // nothing
                break;
            }

            case 2: {
                if (mLArray[left] > mLArray[right]) {
                    swap(left, right);
                }
                break;
            }

            case 3: {
                int middle = (left + right) / 2;
                // left center
                if (mLArray[left] > mLArray[middle]) {
                    swap(left, middle);
                }
                // left right
                if (mLArray[left] > mLArray[right]) {
                    swap(left, right);
                }
                // center right
                if (mLArray[middle] > mLArray[right]) {
                    swap(middle, right);
                }
                break;
            }

            default: {
                System.out.println("cannot occur!");
                break;
            }
        }
    }
}
