package test;

import fireway.ShellSort;
import junit.framework.TestCase;

/**
 * 希尔排序
 * 2018年 07月 21日 星期六
 *
 * @author fireway
 */
public class ShellSortTestCase extends TestCase {
    public void test() {
        ShellSort arr = new ShellSort(20);

        arr.insert(17);
        arr.insert(19);
        arr.insert(14);
        arr.insert(15);
        arr.insert(12);
        arr.insert(18);
        arr.insert(11);
        arr.insert(10);
        arr.insert(16);
        arr.insert(33);
        arr.insert(27);
        arr.insert(29);
        arr.insert(24);
        arr.insert(25);
        arr.insert(22);
        arr.insert(28);
        arr.insert(21);
        arr.insert(20);
        arr.insert(26);
        arr.insert(23);

        arr.display();

        arr.shellSort();

        arr.display();
    }
}
