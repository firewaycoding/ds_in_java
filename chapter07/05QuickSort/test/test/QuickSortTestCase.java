package test;

import junit.framework.TestCase;

import org.junit.Test;

import fireway.QuickSort;

/**
 * 快速排序算法
 * 2018年 08月 05日 星期日
 *
 * @author fireway
 */
public class QuickSortTestCase extends TestCase {
    /**
     * 100个数据逆序
     */
    @Test
    public void test1() {
        int size = 100;
        QuickSort arr = new QuickSort(size);

        for (int i = 0; i < size; i++) {
            long e = size - i;
            arr.insert(e);
        }

        arr.display();

        arr.quickSort();

        arr.display();
    }

    @Test
    public void test2() {
        int size = 10;
        QuickSort arr = new QuickSort(size);

        final long[] TEST = {42, 89, 63, 12, 27, 94, 78, 3, 50, 36};
        for (int i = 0; i < size; i++) {
            long e = TEST[i];
            arr.insert(e);
        }

        arr.display();

        arr.quickSort();

        arr.display();
    }

    /**
     * 测试重复元素
     */
    @Test
    public void testDuplicatArray() {
        int size = 16;
        QuickSort arr = new QuickSort(size);

        final long[] TEST = {144, 89, 132, 162, 50, 188, 176, 164, 89, 193, 50, 7, 129, 73, 50, 191};
        for (int i = 0; i < size; i++) {
            long e = TEST[i];
            arr.insert(e);
        }

        arr.display();

        arr.quickSort();

        arr.display();
    }

    /**
     * 测试重复元素
     */
    @Test
    public void testDuplicatArray2() {
        int size = 10;
        QuickSort arr = new QuickSort(size);

        final long[] TEST = {2, 2, 2, 2, 3, 2, 2, 2, 2, 2};
        for (int i = 0; i < size; i++) {
            long e = TEST[i];
            arr.insert(e);
        }

        arr.display();

        arr.quickSort();

        arr.display();
    }

    @Test
    public void testRandom() {
        int size = 16;
        QuickSort arr = new QuickSort(size);

        for (int i = 0; i < size; i++) {
            long e = (long)(Math.random() * 199);
            arr.insert(e);
        }

        arr.display();

        arr.quickSort();

        arr.display();
    }
}
