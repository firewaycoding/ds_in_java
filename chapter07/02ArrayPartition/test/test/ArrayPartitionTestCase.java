package test;

import org.junit.Test;

import fireway.ArrayPartition;
import junit.framework.TestCase;

/**
 * 划分
 * 2018年 07月 22日 星期日
 *
 * @author fireway
 */
public class ArrayPartitionTestCase extends TestCase {
    @Test
    public void testPartitioning() {
        int size = 16;
        ArrayPartition arr = new ArrayPartition(size);

        for (int i = 0; i < size; i++) {
            long e = (long)(Math.random() * 199);
            arr.insert(e);
        }

        arr.display();

        long pivot = 99;
        int partitionIndex = arr.partitioning(0, size - 1, pivot);
        System.out.println("partition index = " + partitionIndex);

        arr.display();
    }

    @Test
    public void testPartitioningLeft() {
        int size = 15;
        ArrayPartition arr = new ArrayPartition(size);

        for (int i = 0; i < size; i++) {
            long e = (long)(Math.random() * 100);
            arr.insert(e);
        }

        arr.display();

        long pivot = 100;
        int partitionIndex = arr.partitioning(0, size - 1, pivot);
        System.out.println("partition index = " + partitionIndex);

        arr.display();
    }

    @Test
    public void testPartitioningRight() {
        int size = 16;
        ArrayPartition arr = new ArrayPartition(size);

        for (int i = 0; i < size; i++) {
            long e = (long)(Math.random() * 100 + 100);
            arr.insert(e);
        }

        arr.display();

        long pivot = 100;
        int partitionIndex = arr.partitioning(0, size - 1, pivot);
        System.out.println("partition index = " + partitionIndex);

        arr.display();
    }
}
