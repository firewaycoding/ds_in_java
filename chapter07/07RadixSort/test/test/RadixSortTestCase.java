package test;

import fireway.RadixSort;
import junit.framework.TestCase;

/**
 * 基数排序
 * 2018年 08月 10日 星期五
 *
 * @author fireway
 */
public class RadixSortTestCase extends TestCase {
    public void test() {
        final long[] TEST = {421, 240, 35, 532, 305, 430, 124};
        RadixSort arr = new RadixSort(TEST, TEST.length);
        arr.setMaxDigit(3);
        arr.setBase(RadixSort.BASE_10);

        arr.display();
        arr.radixSort();
        arr.display();
    }

    public void testRandom() {
        int size = 16;
        RadixSort arr = new RadixSort(size);
        for (int i = 0; i < size; i++) {
            long e = (long)(Math.random() * 199);
            arr.insert(e);
        }

        arr.setMaxDigit(3);
        arr.setBase(RadixSort.BASE_10);

        arr.display();
        arr.radixSort();
        arr.display();
    }
}
