import org.junit.Before;
import org.junit.Test;

/**
 * 图的深度优先搜索
 *
 * 2018年 12月 12日 星期三
 * @author fireway  
 */
public class GraphTest {
    Graph mGraph;

    @Before
    public void setUp() throws Exception {
        mGraph = new Graph();
    }

    @Test
    public void testGraph() {
        System.out.println(mGraph);
    }

    @Test
    public void testAddVertex() {
        mGraph.addVertex('A');
        mGraph.addVertex('B');
        mGraph.addVertex('C');
        mGraph.addVertex('D');
        mGraph.addVertex('E');
        mGraph.addVertex('F');
        mGraph.addVertex('G');
        mGraph.addVertex('H');
    }

    @Test
    public void testAddEdge() {
        testAddVertex();
        mGraph.addEdge(0, 3); // AD
        mGraph.addEdge(0, 4); // AE
        mGraph.addEdge(1, 4); // BE
        mGraph.addEdge(2, 5); // CF
        mGraph.addEdge(3, 6); // DG
        mGraph.addEdge(4, 6); // EG
        mGraph.addEdge(5, 7); // FH
        mGraph.addEdge(6, 7); // GH
    }

    @Test
    public void testDfs() {
        testAddVertex();
        testAddEdge();
        mGraph.dfs();
    }
}

