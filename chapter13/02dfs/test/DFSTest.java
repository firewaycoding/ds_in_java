import org.junit.Test;

/**
 * 图的深度优先搜索
 *
 * 2018年 12月 12日 星期三
 * @author fireway  
 */
public class DFSTest {
    @Test
    public void test() {
        Graph theGraph = new Graph();
        theGraph.addVertex('A'); // 0
        theGraph.addVertex('B'); // 1
        theGraph.addVertex('C'); // 2
        theGraph.addVertex('D'); // 3
        theGraph.addVertex('E'); // 4

        theGraph.addEdge(0, 1); // AB
        theGraph.addEdge(1, 2); // BC
        theGraph.addEdge(0, 3); // AD
        theGraph.addEdge(3, 4); // DE

        System.out.print("Visits: ");
        theGraph.dfs(); // depth-first search
        System.out.println();

        System.out.print("Visits: ");
        theGraph.dfs(2); // depth-first search
        System.out.println();

        System.out.print("Visits: ");
        theGraph.dfs(4); // depth-first search
        System.out.println();
    }
}

