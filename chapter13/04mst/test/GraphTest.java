import org.junit.Test;

public class GraphTest {
	/**
	 * 最小生成树测试
	 */
	@Test
	public void test() {
	      Graph g = new Graph();
	      g.addVertex('A');    // 0 
	      g.addVertex('B');    // 1
	      g.addVertex('C');    // 2
	      g.addVertex('D');    // 3
	      g.addVertex('E');    // 4

	      g.addEdge(0, 1);     // AB
	      g.addEdge(0, 2);     // AC
	      g.addEdge(0, 3);     // AD
	      g.addEdge(0, 4);     // AE
	      g.addEdge(1, 2);     // BC
	      g.addEdge(1, 3);     // BD
	      g.addEdge(1, 4);     // BE
	      g.addEdge(2, 3);     // CD
	      g.addEdge(2, 4);     // CE
	      g.addEdge(3, 4);     // DE

	      System.out.print("Minimum spanning tree: ");
	      g.mst();       
	      System.out.println();
	}
}

