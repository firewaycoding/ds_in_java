import org.junit.Test;

/**
 * 广度优先搜索BFS
 * 2018年 12月 14日 星期五
 *
 * @author fireway
 */
public class GraphTest {
    @Test
    public void testBFS1() {
        Graph g = new Graph();
        g.addVertex('a');
        g.addVertex('b');
        g.addVertex('c');
        g.addVertex('d');
        g.addVertex('e');
        g.addVertex('f');
        g.addVertex('g');
        g.addVertex('h');
        g.addVertex('i');

        // a
        g.addEdge(0, 1);
        g.addEdge(0, 3);
        g.addEdge(0, 4);

        // b
        g.addEdge(1, 0);
        g.addEdge(1, 2);
        g.addEdge(1, 3);
        g.addEdge(1, 4);
        g.addEdge(1, 5);

        // c
        g.addEdge(2, 1);
        g.addEdge(2, 4);
        g.addEdge(2, 5);

        // d
        g.addEdge(3, 0);
        g.addEdge(3, 1);
        g.addEdge(3, 4);
        g.addEdge(3, 6);
        g.addEdge(3, 7);

        // e
        g.addEdge(4, 0);
        g.addEdge(4, 1);
        g.addEdge(4, 2);
        g.addEdge(4, 3);
        g.addEdge(4, 5);
        g.addEdge(4, 6);
        g.addEdge(4, 7);
        g.addEdge(4, 8);

        // f
        g.addEdge(5, 1);
        g.addEdge(5, 2);
        g.addEdge(5, 4);
        g.addEdge(5, 7);
        g.addEdge(5, 8);

        // g
        g.addEdge(6, 3);
        g.addEdge(6, 4);
        g.addEdge(6, 7);

        // h
        g.addEdge(7, 3);
        g.addEdge(7, 4);
        g.addEdge(7, 5);
        g.addEdge(7, 6);
        g.addEdge(7, 8);

        // i
        g.addEdge(8, 4);
        g.addEdge(8, 5);
        g.addEdge(8, 7);

        System.out.print("Visit: ");
        g.bfs();
        System.out.println();
    }

    @Test
    public void testBFS2() {
        Graph g = new Graph();
        g.addVertex('A');    // 0
        g.addVertex('B');    // 1
        g.addVertex('C');    // 2
        g.addVertex('D');    // 3
        g.addVertex('E');    // 4

        g.addEdge(0, 1);     // AB
        g.addEdge(1, 2);     // BC
        g.addEdge(0, 3);     // AD
        g.addEdge(3, 4);     // DE

        System.out.print("Visits: ");
        g.bfs();
        System.out.println();
    }

    @Test
    public void testQueue() {
        Queue q = new Queue();
        for (int i = 0; i < 33; i++) {
            q.insert(i);
        }

        for (int i = 0; i < 33; i++) {
            System.out.println(i + " --> " + q.remove() + ", isEmpty ? "
                               + q.isEmpty());
        }
    }
}

