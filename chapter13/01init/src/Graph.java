
/**
 * <p>顶点类对象表示一个顶点，可能会包含很多信息</p>
 * <p>示例程序中仅存储一个字母，例如A用来识别顶点</p>
 *
 * 2018年 12月 09日 星期日
 * @author fireway
 */
class Vertex {
    public char mLabel;

    public Vertex(char lab) {
        mLabel = lab;
    }
}

/**
 * 无向图
 */
class Graph {
    private final int MAX_VERTS = 20;
    // 顶点对象放在数组中维护
    private Vertex[] mVertexList;
    private int[][] mAdjMat;
    // 当前顶点的个数
    private int mVertNum;

    public Graph() {
        mVertNum = 0;
        mVertexList = new Vertex[MAX_VERTS];
        // adjacency matrix - 邻接矩阵
        mAdjMat = new int[MAX_VERTS][MAX_VERTS];
        for (int i = 0; i < MAX_VERTS; i++) {
            for (int j = 0; j < MAX_VERTS; j++) {
                mAdjMat[i][j] = 0;
            }
        }
    }

    /**
     * 添加顶点
     */
    public void addVertex(char lab) {
        mVertexList[mVertNum++] = new Vertex(lab);
    }

    /**
     * 添加边
     */
    public void addEdge(int start, int end) {
        mAdjMat[start][end] = 1;
        mAdjMat[end][start] = 1;
    }
}

