/**
 * 插入冒泡排序
 * 2018年 03月 16日 星期五
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class InsertSortTest extends TestCase {

    public void test() {
        InsertSort arr = new InsertSort(10);

        arr.insert(77);
        arr.insert(99);
        arr.insert(44);
        arr.insert(55);
        arr.insert(22);
        arr.insert(88);
        arr.insert(11);
        arr.insert(00);
        arr.insert(66);
        arr.insert(33);

        arr.display();

        arr.insertSort();

        arr.display();
    }

    public void test2() {
        InsertSort arr = new InsertSort(10);

        arr.insert(00);
        arr.insert(11);
        arr.insert(22);
        arr.insert(33);
        arr.insert(44);
        arr.insert(55);
        arr.insert(66);
        arr.insert(77);
        arr.insert(88);
        arr.insert(99);

        arr.display();

        arr.insertSort();

        arr.display();
    }

    public void test3() {
        InsertSort arr = new InsertSort(10);

        arr.insert(99);
        arr.insert(88);
        arr.insert(77);
        arr.insert(66);
        arr.insert(55);
        arr.insert(44);
        arr.insert(33);
        arr.insert(22);
        arr.insert(11);
        arr.insert(00);

        arr.display();

        arr.insertSort();

        arr.display();
    }

}

