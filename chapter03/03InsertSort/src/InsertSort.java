import java.util.Arrays;

/**
 * 插入排序
 * 2018年 03月 16日 星期五
 *
 * @author fireway
 */
public class InsertSort {
    private long mLArray[];

    private int mSize;

    public InsertSort(int initialCapacity) {
        mLArray = new long[initialCapacity];
        mSize = 0;
    }

    public void insert(long value) {
        mLArray[mSize++] = value;
    }

    public void display() {
        System.out.print("[");
        for (int i = 0; i < mSize; i++) {
            System.out.print(mLArray[i]);
            if (i != mSize - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");
    }

    public void insertSort() {
        for (int i = 1; i < mSize; i++) {
            String order = String.format("%2s", i);
            System.out.print("order" + order + " : ");

            long tmp = mLArray[i];
            int j = i;
            while (j > 0 && mLArray[j - 1] >= tmp) {
                mLArray[j] = mLArray[j - 1];
                j--;
            }

            mLArray[j] = tmp;

            System.out.print(Arrays.toString(mLArray));
            System.out.println("");
        }
    }
}

