/**
 * 2018年 03月 20日 星期二
 *
 * @author fireway
 */
public class Person implements Comparable<Person> {
    private String mLastName;

    private String mFirstName;

    private int mAge;

    public Person(String lastName, String firstName, int age) {
        mLastName = lastName;
        mFirstName = firstName;
        mAge = age;
    }

    public Person(String lastName, String firstName) {
        mLastName = lastName;
        mFirstName = firstName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Person) {
            if (obj != null) {
                if (((Person) obj).mLastName.equals(mLastName)
                        && ((Person) obj).mFirstName.equals(mFirstName)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
//        sb.append("{ ");
//        sb.append("Last name: " + mLastName);
//        sb.append(", First name: " + mFirstName);
//        sb.append(", Age: " + mAge);
//        sb.append(" }");

        sb.append(mLastName);
        return sb.toString();
    }

    @Override
    public int compareTo(Person anotherPerson) {
        return mLastName.compareTo(anotherPerson.mLastName);
    }
}
