/**
 * 2018年 03月 20日 星期二
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class ObjectArrayTest extends TestCase {
    public void test1() {
        ObjectArray<Person> persons = new ObjectArray<Person>(10);

        Person[] ps = {
            new Person("Yee", "Tom", 43),
            new Person("Evans", "Patty", 24),
            new Person("Adams", "Henry", 63),
            new Person("Hashimoto", "Sato", 21),
            new Person("Stimson", "Henry", 29),
            new Person("Velasquez", "Jose", 72),
            new Person("Lamarque", "Henry", 54),
            new Person("Vang", "Minh", 22),
            new Person("Creswell", "Lucinda", 18),
            new Person("Wei", "Fireway", 18)
        };

        for (Person p : ps) {
            persons.insert(p);
        }

        System.out.println("size = " + persons.size());
        System.out.println(persons.toString());

        persons.insertSort();

        System.out.println(persons.toString());
    }

    public void test2() {
        Person p1 = new Person("Adams", "Tom", 43);
        Person p2 = new Person("Hashimoto", "Patty", 24);
        int res = p1.compareTo(p2);
        System.out.println("result = " + res);
    }

    public void test3() {
        Person p1 = new Person("Adams", "Tom", 43);
        Person p2 = new Person("Hashimoto", "Patty", 24);
        int res = p2.compareTo(p1);
        System.out.println("result = " + res);
    }
}

