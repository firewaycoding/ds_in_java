/**
 * 选择排序
 * 2018年 03月 13日 星期二
 *
 * @author fireway
 */
import java.util.Arrays;

public class SelectSort {
    private long mLArray[];

    private int mSize;

    public SelectSort(int initialCapacity) {
        mLArray = new long[initialCapacity];
        mSize = 0;
    }

    public void insert(long value) {
        mLArray[mSize++] = value;
    }

    public void display() {
        System.out.print("[");
        for (int i = 0; i < mSize; i++) {
            System.out.print(mLArray[i]);
            if (i != mSize -1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");
    }

    public void selectSort() {
        for (int i = 0; i < mSize - 1; i++) {  // 趟数(N - 1)
            String order = String.format("%2s", (i + 1));
            System.out.print("order" + order + " : ");
            int min = i;
            for (int j = i + 1; j < mSize; j++) {
                if (mLArray[j] < mLArray[min]) {
                    min = j;
                }
            }

            System.out.print(Arrays.toString(mLArray));
            System.out.println("");
            // swap
            long tmp = mLArray[i];
            mLArray[i] = mLArray[min];
            mLArray[min] = tmp;
        }
    }
}

