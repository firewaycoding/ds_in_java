/**
 * 测试选择排序
 * 2018年 03月 13日 星期二
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class SelectSortTest extends TestCase {

    public void test() {
        SelectSort arr = new SelectSort(10);

        arr.insert(7);
        arr.insert(9);
        arr.insert(4);
        arr.insert(5);
        arr.insert(2);
        arr.insert(8);
        arr.insert(1);
        arr.insert(0);
        arr.insert(6);
        arr.insert(3);

        arr.display();

        arr.selectSort();

        arr.display();
    }

    public void test2() {
        SelectSort arr = new SelectSort(10);

        arr.insert(0);
        arr.insert(1);
        arr.insert(2);
        arr.insert(3);
        arr.insert(4);
        arr.insert(5);
        arr.insert(6);
        arr.insert(7);
        arr.insert(8);
        arr.insert(9);

        arr.display();

        arr.selectSort();

        arr.display();
    }

    public void test3() {
        SelectSort arr = new SelectSort(10);

        arr.insert(9);
        arr.insert(8);
        arr.insert(7);
        arr.insert(6);
        arr.insert(5);
        arr.insert(4);
        arr.insert(3);
        arr.insert(2);
        arr.insert(1);
        arr.insert(0);

        arr.display();

        arr.selectSort();

        arr.display();
    }

}

