/**
 * 对100万条long数据进行升序排序，几种简单排序之间的比较。
 * 2018年 03月 22日 星期四
 *
 * @author fireway
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import junit.framework.TestCase;

public class LongArrayTest extends TestCase {
    private static final int SIZE = 1000000;
    /**
     * 产生随机数，写入文本
     */
    public void testRandomToTxt() {
        LongArray arr = new LongArray(SIZE);

        for (int i = 0; i < SIZE; i++) {
            long a = (long) (Math.random() * (SIZE - 1));
            arr.insert(a);
        }

        arr.display();

        // 导出到文本里
        FileWriter fw = null;
        try {
            fw = new FileWriter(new File("gen.txt"));
            for (Long a : arr) {
                fw.write(Long.toString(a.longValue()));
                fw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 产生升序，写入文本
     */
    public void testAscToTxt() {
        FileWriter fw = null;
        try {
            fw = new FileWriter(new File("asc.txt"));
            for (int i = 0; i < SIZE; i++) {
                fw.write(Long.toString(i + 1));
                fw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 产生降序，写入文本
     */
    public void testDescToTxt() {
        FileWriter fw = null;
        try {
            fw = new FileWriter(new File("desc.txt"));
            for (int i = SIZE; i > 0; i--) {
                fw.write(Long.toString(i));
                fw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeToTxt(LongArray arr, String fileName) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(new File(fileName));
            for (Long a : arr) {
                fw.write(Long.toString(a.longValue()));
                fw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private LongArray readFromTxt(String fileName) {
        FileReader fr = null;
        BufferedReader br = null;

        try {
            fr = new FileReader(new File(fileName));
            br = new BufferedReader(fr);
            String s = null;
            LongArray arr = new LongArray(SIZE);
            while ((s = br.readLine()) != null) {
                long a = Long.parseLong(s);
                arr.insert(a);
            }
            return arr;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public void test() {
        String fileName = "desc.txt";
        LongArray arr1 = readFromTxt(fileName);
        // arr1.display();
        long startTime = System.nanoTime();
        arr1.bubbleSort();
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("BubbleSort time: " + estimatedTime);
        writeToTxt(arr1, "bubble.txt");

        LongArray arr2 = readFromTxt(fileName);
        // arr2.display();
        startTime = System.nanoTime();
        arr2.selectSort();
        estimatedTime = System.nanoTime() - startTime;
        System.out.println("SelectSort time: " + estimatedTime);
        writeToTxt(arr2, "select.txt");

        LongArray arr3 = readFromTxt(fileName);
        // arr3.display();
        startTime = System.nanoTime();
        arr3.insertSort();
        estimatedTime = System.nanoTime() - startTime;
        System.out.println("InsertSort time: " + estimatedTime);
        writeToTxt(arr3, "insert.txt");
    }

    /**
     * 测试foreach
     */
    public void test2() {
        LongArray arr = new LongArray(10);
        for (int i = 0; i < 10; i++) {
            arr.insert(i + 1);
        }
        arr.display();
        for(Long  a : arr) {
            System.out.println(a.longValue());
        }
    }

    /**
     * 测试remove
     */
    public void test3() {
        LongArray arr = new LongArray(10);
        for (int i = 0; i < 10; i++) {
            arr.insert(i + 1);
        }
        arr.display();
        arr.remove(3);
        arr.display();
    }
}
