/**
 * 测试冒泡排序
 * 2018年 03月 12日 星期一
 *
 * @author fireway
 */
import junit.framework.TestCase;

public class BubbleSortTest extends TestCase {

    public void test() {
        BubbleSort arr = new BubbleSort(10);

        arr.insert(77);
        arr.insert(99);
        arr.insert(44);
        arr.insert(55);
        arr.insert(22);
        arr.insert(88);
        arr.insert(11);
        arr.insert(00);
        arr.insert(66);
        arr.insert(33);

        arr.display();

        arr.bubbleSort2();

        arr.display();
    }

    public void test2() {
        BubbleSort arr = new BubbleSort(10);

        arr.insert(00);
        arr.insert(11);
        arr.insert(22);
        arr.insert(33);
        arr.insert(44);
        arr.insert(55);
        arr.insert(66);
        arr.insert(77);
        arr.insert(88);
        arr.insert(99);

        arr.display();

        arr.bubbleSort2();

        arr.display();
    }

    public void test3() {
        BubbleSort arr = new BubbleSort(10);

        arr.insert(99);
        arr.insert(88);
        arr.insert(77);
        arr.insert(66);
        arr.insert(55);
        arr.insert(44);
        arr.insert(33);
        arr.insert(22);
        arr.insert(11);
        arr.insert(00);

        arr.display();

        arr.bubbleSort2();

        arr.display();
    }

}

