import junit.framework.TestCase;

/**
 * 哈夫曼编码(The Huffman Code)
 * 2018年 09月 02日 星期日
 *
 * @author fireway
 */
public class HuffmanTest extends TestCase {
    public void test1() {
        String test = "SUSIE SAYS IT IS EASY\n";
        String res = Huffman.encode(test);
        System.out.println(res);
        res = Huffman.decode(res);
        System.out.println(res);
        assertEquals(test, res);
    }

    public void test2() {
        String test = "This document is the API specification for the Java Platform, Standard Edition.";
        String res = Huffman.encode(test);
        System.out.println(res);
        res = Huffman.decode(res);
        System.out.println(res);
        assertEquals(test, res);
    }
}
