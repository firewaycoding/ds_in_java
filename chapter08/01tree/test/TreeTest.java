import org.junit.Test;

public class TreeTest {
    @Test
    public void testfullBinaryTree() {
        Tree tree = new Tree();
        // 第0层
        tree.insert(50, 0);
        // 第1层
        tree.insert(25, 0);
        tree.insert(75, 0);
        // 第2层
        tree.insert(12, 0);
        tree.insert(37, 0);
        tree.insert(63, 0);
        tree.insert(87, 0);
        // 第3层
        tree.insert(6, 0);
        tree.insert(18, 0);
        tree.insert(31, 0);
        tree.insert(43, 0);
        tree.insert(57, 0);
        tree.insert(69, 0);
        tree.insert(81, 0);
        tree.insert(93, 0);
        // 第4层
        tree.insert(3, 0);
        tree.insert(9, 0);
        tree.insert(14, 0);
        tree.insert(22, 0);
        tree.insert(27, 0);
        tree.insert(35, 0);
        tree.insert(39, 0);
        tree.insert(47, 0);
        tree.insert(51, 0);
        tree.insert(60, 0);
        tree.insert(64, 0);
        tree.insert(71, 0);
        tree.insert(77, 0);
        tree.insert(84, 0);
        tree.insert(90, 0);
        tree.insert(95, 0);
        tree.displayTree();
    }

    @Test
    public void testFind() {
        Tree tree = new Tree();
        tree.insert(63, 0);
        tree.insert(27, 0);
        tree.insert(80, 0);
        tree.insert(13, 0);
        tree.insert(51, 0);
        tree.insert(70, 0);
        tree.insert(92, 0);
        tree.insert(26, 0);
        tree.insert(33, 0);
        tree.insert(58, 0);
        tree.insert(82, 0);
        tree.insert(57, 0);
        tree.insert(60, 0);
        tree.displayTree();
        Node node = tree.find(57);
        System.out.println(node);
    }

    @Test
    public void testTraversing() {
        Tree tree = new Tree();
        tree.insert(63, 0);
        tree.insert(27, 0);
        tree.insert(80, 0);
        tree.insert(13, 0);
        tree.insert(51, 0);
        tree.insert(70, 0);
        tree.insert(92, 0);
        tree.insert(26, 0);
        tree.insert(33, 0);
        tree.insert(58, 0);
        tree.insert(82, 0);
        tree.insert(57, 0);
        tree.insert(60, 0);
        tree.displayTree();
        System.out.print("Preorder : ");
        tree.traverse(TraverseType.Preorder);
        System.out.println("");
        System.out.print("Inorder  : ");
        tree.traverse(TraverseType.Inorder);
        System.out.println("");
        System.out.print("Postorder: ");
        tree.traverse(TraverseType.Postorder);
        System.out.println("");
    }

    @Test
    public void testGetMaxAndMin() {
        Tree tree = new Tree();
        tree.insert(63, 0);
        tree.insert(27, 0);
        tree.insert(80, 0);
        tree.insert(13, 0);
        tree.insert(51, 0);
        tree.insert(70, 0);
        tree.insert(92, 0);
        tree.insert(26, 0);
        tree.insert(33, 0);
        tree.insert(58, 0);
        tree.insert(82, 0);
        tree.insert(57, 0);
        tree.insert(60, 0);
        tree.displayTree();
        Node node = tree.getMininum();
        System.out.println(node.mKey);
        node = tree.getMaximum();
        System.out.println(node.mKey);
    }

    /**
     * 删除叶结点
     */
    @Test
    public void testDeleteANodeWithNoChild() {
        Tree tree = new Tree();
        tree.insert(63, 0);
        tree.insert(27, 0);
        tree.insert(80, 0);
        tree.insert(13, 0);
        tree.insert(51, 0);
        tree.insert(70, 0);
        tree.insert(92, 0);
        tree.insert(26, 0);
        tree.insert(33, 0);
        tree.insert(58, 0);
        tree.insert(82, 0);
        tree.insert(57, 0);
        tree.insert(60, 0);
        tree.displayTree();
        boolean b = tree.delete(57);
        System.out.println("b = " + b);
        tree.displayTree();
    }

    /**
     * 删除有一个子结点的结点
     */
    @Test
    public void testDeleteANodeWithOneChildCase1() {
        Tree tree = new Tree();
        tree.insert(63, 0);
        tree.insert(27, 0);
        tree.insert(80, 0);
        tree.insert(13, 0);
        tree.insert(51, 0);
        tree.insert(70, 0);
        tree.insert(92, 0);
        tree.insert(26, 0);
        tree.insert(33, 0);
        tree.insert(58, 0);
        tree.insert(65, 0);
        tree.insert(82, 0);
        tree.insert(57, 0);
        tree.insert(60, 0);
        tree.displayTree();
        boolean b = tree.delete(70);
        System.out.println("b = " + b);
        tree.displayTree();
    }

    /**
     * 删除有一个子结点的结点
     */
    @Test
    public void testDeleteANodeWithOneChildCase2() {
        Tree tree = new Tree();
        tree.insert(63, 0);
        tree.insert(27, 0);
        tree.insert(80, 0);
        tree.insert(13, 0);
        tree.insert(51, 0);
        tree.insert(70, 0);
        tree.insert(92, 0);
        tree.insert(26, 0);
        tree.insert(33, 0);
        tree.insert(58, 0);
        tree.insert(75, 0);
        tree.insert(82, 0);
        tree.insert(57, 0);
        tree.insert(60, 0);
        tree.insert(74, 0);
        tree.insert(76, 0);
        tree.displayTree();
        boolean b = tree.delete(70);
        System.out.println("b = " + b);
        tree.displayTree();
    }

    /**
     * 删除有两个子结点的结点
     * 后继结点是current的右子结点
     */
    @Test
    public void testDeleteANodeWithTwoChildCase1() {
        Tree tree = new Tree();
        // 第0层
        tree.insert(50, 0);
        // 第1层
        tree.insert(25, 0);
        tree.insert(75, 0);
        // 第2层
        tree.insert(12, 0);
        tree.insert(37, 0);
        tree.insert(63, 0);
        tree.insert(87, 0);
        // 第3层
        tree.insert(6, 0);
        tree.insert(18, 0);
        tree.insert(31, 0);
        tree.insert(43, 0);
        tree.insert(57, 0);
        tree.insert(69, 0);
        tree.insert(93, 0);
        // 第4层
        tree.insert(3, 0);
        tree.insert(9, 0);
        tree.insert(14, 0);
        tree.insert(22, 0);
        tree.insert(27, 0);
        tree.insert(35, 0);
        tree.insert(39, 0);
        tree.insert(47, 0);
        tree.insert(51, 0);
        tree.insert(60, 0);
        tree.insert(64, 0);
        tree.insert(71, 0);
        tree.insert(90, 0);
        tree.insert(95, 0);
        tree.displayTree();
        boolean b = tree.delete(75);
        System.out.println("b = " + b);
        tree.displayTree();
    }

    /**
     * 删除有两个子结点的结点
     * 后继结点是current的右子结点的左子孙结点
     */
    @Test
    public void testDeleteANodeWithTwoChildCase2() {
        Tree tree = new Tree();
        // 第0层
        tree.insert(50, 0);
        // 第1层
        tree.insert(25, 0);
        tree.insert(75, 0);
        // 第2层
        tree.insert(12, 0);
        tree.insert(37, 0);
        tree.insert(63, 0);
        tree.insert(87, 0);
        // 第3层
        tree.insert(6, 0);
        tree.insert(18, 0);
        tree.insert(31, 0);
        tree.insert(43, 0);
        tree.insert(57, 0);
        tree.insert(69, 0);
        tree.insert(81, 0);
        tree.insert(93, 0);
        // 第4层
        tree.insert(3, 0);
        tree.insert(9, 0);
        tree.insert(14, 0);
        tree.insert(22, 0);
        tree.insert(27, 0);
        tree.insert(35, 0);
        tree.insert(39, 0);
        tree.insert(47, 0);
        tree.insert(51, 0);
        tree.insert(60, 0);
        tree.insert(64, 0);
        tree.insert(71, 0);
        tree.insert(77, 0);
        tree.insert(84, 0);
        tree.insert(90, 0);
        tree.insert(95, 0);
        // 第5层
        tree.insert(29, 0);
        tree.displayTree();
        boolean b = tree.delete(25);
        System.out.println("b = " + b);
        tree.displayTree();
    }
}
